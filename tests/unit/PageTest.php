<?php

use \Fletch\Entities\Page;
use \Fletch\ELements\ElementLibrary;

class PageTest extends \PHPUnit_Framework_TestCase
{
    /** @var  Fletch\Entities\Page */
    protected $page;

    protected function setUp()
    {
        $mock = $this->getMockBuilder('\Fletch\Entities\Page')
                        ->setMethods(null)
                        ->getMock();
        
        $elements = array();
        $elements[0] = new stdClass();
        $elements[0]->id = '0';
        $elements[0]->region = '0';
        $elements[0]->position = '0';
        $elements[0]->page_id = '0';
        $elements[0]->type = 'div';
        $elements[0]->created_at = '2014-08-07 20:20:17';
        $elements[0]->updated_at = '2014-08-07 20:20:17';
        $elements[0]->drafts = new stdClass();
        $elements[0]->drafts->region = '3';
        $elements[0]->drafts->position = '3';
        $elements[0]->content = array();
        $elements[0]->content[0] = new stdClass();
        $elements[0]->content[0]->data = 'Hello World';
        $elements[0]->content[0]->drafts = new stdClass();
        $elements[0]->content[0]->drafts->data = 'Goodbye World';
        $elements[0]->content[1] = new stdClass();
        $elements[0]->content[1]->data = 'Hello Earth';
        
        $mock->elements = $elements;

        $elementLibrary = $this->getMock('Fletch\Elements\ElementLibrary');
        $elementLibrary->elements = array('div' => 'stdClass');
        $mock->elementLibrary = $elementLibrary;


        $this->page = $mock;
    }

    protected function tearDown()
    {
    }

    /**
     * Test that we can register an element
     */
    public function testGetElementsByRegion()
    {

        // Call the function
        $elements = $this->page->getElementsByRegion(0);

        $expected_return = ['0' => new stdClass()];
        
        // Make comparison
        $this->assertEquals($expected_return, $elements);
    }

    /**
     * Test that we can register an element
     */
    public function testGetHtml()
    {
        // Relies almost entirely on other methods. We'll test this as a functional test.
    }

}