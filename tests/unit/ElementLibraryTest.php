<?php

class ElementLibraryTest extends \PHPUnit_Framework_TestCase
{
    /** @var  Fletch\Elements\ElementLibrary */
    protected $elementLibrary;

    protected function setUp()
    {
        $this->elementLibrary = new Fletch\Elements\ElementLibrary;
    }

    protected function tearDown()
    {
    }

    /**
     * Test that we can register an element
     */
    public function testRegisterElement()
    {
        // Create the array we'll use
        $myElement = new \Fletch\Elements\ElementLibrary;
        $myElement->slug = 'my_element';

        // Call the function
        $this->elementLibrary->registerElement($myElement);

        // Make comparison
        $this->assertArrayHasKey('my_element', $this->elementLibrary->elements);
    }

}