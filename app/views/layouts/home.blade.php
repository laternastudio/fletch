<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width" />
	<title>@yield('title') | Example Site</title>

	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700, 700italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="{{ URL::asset('css/fletch.css') }}" />
	<link rel="stylesheet" href="{{ URL::asset('css/theme.css') }}" />
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="{{ URL::asset('js/main.min.js') }}"></script>

	<script type="text/javascript" src="http://updateyourbrowser.net/uyb.js"> </script>

	<!-- Livereload for Gruntjs -->
	<script src="http://localhost:35729/livereload.js"></script>
</head>

<body>

	
	@if (!Auth::guest())
		{{-- Render the page info has hidden divs --}}
		@if(isset($page))
			@include('ui.pageInfo', array('page'=>$page)) 
		@endif
		
		{{-- Render the site's global settings as hidden divs --}}
		@if(isset($settings))
			@include('ui.settingsInfo', array('settings'=>$settings)) 
		@endif
	
		{{-- Set up the upper tool and notification bar --}}
		@include('ui.bar') 
	@endif


	{{-- Set up the header --}}
	<header class="container primary">
		<div class="row" style="background-color: white; padding-top: 1em; ">

				<a href="{{ URL::to('/') }}" class="main-logo">
					<img src="{{ URL::asset('img/fletch_logo.png') }}" />
				</a>

				<!-- Desktop Navigation -->
				<nav class="desktop">
					<ul class="navigation">
						{{ Render::navigation('', $page) }}
					</ul>
					<ul class="socialmedia">
						{{ Render::socialmedia() }}
					</ul>
				</nav>


				<!-- Mobile Navigation -->
				<a id="mobile-nav-trigger" class="mobile lines-button x">
				  <span class="lines"></span>
				</a>

				<nav class="mobile clearfix">
					<ul class="navigation">
						{{ Render::navigation('', $page) }}
					</ul>
					<ul class="socialmedia">
						{{ Render::socialmedia() }}
					</ul>					
				</nav>
		</div>
	</header>

	{{-- Set up the main content area for the site --}}
	<div class="container main-content-area">
		<div class="row" style="background-color: white; padding-top: 1em; ">

				@yield('content')

		</div>
	</div>
 


	@if (!Auth::guest())
		{{-- Load up the JS files needed for the CMS --}}
		<script src="{{ URL::asset('js/ckeditor/ckeditor.js') }}"></script>
		<script src="{{ URL::asset('js/user.min.js') }}"></script>

		{{-- This panel is used for current page settings and more --}}
		@include('ui.settingsPanel') 

		{{-- This is the dropdown "Tools" menu --}}
		@include('ui.tools')

		{{-- This is the menu that appears when you click "Add an Item" --}}
		@include('ui.creator')

		{{-- Render the site's global settings as a JS array --}}
		@if(isset($reserved_routes))
			@include('ui.reservedRoutes', array('reserved_routes'=>$reserved_routes)) 
		@endif
	@endif


</body>
</html>
