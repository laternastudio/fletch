@extends('layouts.home')

@section('title')
	{{ (isset($page->long_title) && $page->long_title != '') ? $page->long_title : $page->title }}
@endsection
