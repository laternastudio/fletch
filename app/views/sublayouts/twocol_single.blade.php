@extends('layouts.default')

@section('title')
	{{ (isset($page->long_title) && $page->long_title != '') ? $page->long_title : $page->title }}
@endsection

@section('content')
	<div class="row">
		<div class="twelvecol last region" id="region-0" data-region="0">
			@if(!Auth::guest())<ul class="fl-region-list">@endif
				{{ isset($page->target[0]) ? $page->target[0] : '' }}
			@if(!Auth::guest())</ul>@endif
		</div>
	</div>


	<div class="row">
		<div class="sixcol region" id="region-1" data-region="1">
            @if(!Auth::guest())<ul class="fl-region-list">@endif
				{{ isset($page->target[1]) ? $page->target[1] : '' }}
			@if(!Auth::guest())</ul>@endif
		</div>
		<div class="sixcol last region" id="region-2" data-region="2">
			@if(!Auth::guest())<ul class="fl-region-list">@endif
				{{ isset($page->target[2]) ? $page->target[2] : '' }}
			@if(!Auth::guest())</ul>@endif
		</div>

	</div>

@endsection