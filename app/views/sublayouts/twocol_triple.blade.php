@extends('layouts.default')

@section('title')
	{{ (isset($page->long_title) && $page->long_title != '') ? $page->long_title : $page->title }}
@endsection

@section('content')
	<div class="row">
		<div class="twelvecol last region" id="region-0" data-region="0">
			@if(!Auth::guest())<ul class="fl-region-list">@endif
				{{ isset($page->target[0]) ? $page->target[0] : '' }}
			@if(!Auth::guest())</ul>@endif
		</div>
	</div>


	<div class="row">
		<div class="sixcol region" id="region-1" data-region="1">
            @if(!Auth::guest())<ul class="fl-region-list">@endif
				{{ isset($page->target[1]) ? $page->target[1] : '' }}
			@if(!Auth::guest())</ul>@endif
		</div>
		<div class="sixcol last region" id="region-2" data-region="2">
			@if(!Auth::guest())<ul class="fl-region-list">@endif
				{{ isset($page->target[2]) ? $page->target[2] : '' }}
			@if(!Auth::guest())</ul>@endif
		</div>
	</div>

	{{-- The Start of the second section --}}
	<div class="row">
		<div class="twelvecol last region" id="region-3" data-region="3">
			@if(!Auth::guest())<ul class="fl-region-list">@endif
				{{ isset($page->target[3]) ? $page->target[3] : '' }}
			@if(!Auth::guest())</ul>@endif
		</div>
	</div>

	<div class="row">
		<div class="sixcol region" id="region-4" data-region="4">
            @if(!Auth::guest())<ul class="fl-region-list">@endif
				{{ isset($page->target[4]) ? $page->target[4] : '' }}
			@if(!Auth::guest())</ul>@endif
		</div>
		<div class="sixcol last region" id="region-5" data-region="5">
			@if(!Auth::guest())<ul class="fl-region-list">@endif
				{{ isset($page->target[5]) ? $page->target[5] : '' }}
			@if(!Auth::guest())</ul>@endif
		</div>
	</div>

	{{-- The Start of the second section --}}
	<div class="row">
		<div class="twelvecol last region" id="region-6" data-region="6">
			@if(!Auth::guest())<ul class="fl-region-list">@endif
				{{ isset($page->target[6]) ? $page->target[6] : '' }}
			@if(!Auth::guest())</ul>@endif
		</div>
	</div>

	<div class="row">
		<div class="sixcol region" id="region-7" data-region="7">
            @if(!Auth::guest())<ul class="fl-region-list">@endif
				{{ isset($page->target[7]) ? $page->target[7] : '' }}
			@if(!Auth::guest())</ul>@endif
		</div>
		<div class="sixcol last region" id="region-8" data-region="8">
			@if(!Auth::guest())<ul class="fl-region-list">@endif
				{{ isset($page->target[8]) ? $page->target[8] : '' }}
			@if(!Auth::guest())</ul>@endif
		</div>
	</div>

@endsection