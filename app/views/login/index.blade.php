@extends('layouts.default')

@section('title')
	Login
@endsection

@section('content')

    <div class="row">
        <div class="twelvecol">
            <h1 style="text-align: center;">Enter your login credentials</h1>
        </div>
    </div>

    <div class="row">
        <div class="fourcol"></div>
        <div class="fourcol">

        	<form method="POST" action="{{ URL::to('/login') }}" accept-charset="UTF-8">  

                <!-- check for login errors flash var -->
                @if (Session::has('login_errors'))
                    <span class="error">Username or password incorrect.</span>
                @endif

                <!-- username field -->
                <p><label for="username">Username</label></p>
                <p><input class="theme-text" type="text" name="username" id="username"></p>

                <!-- password field -->
                <p><label for="password">Password</label></p>
                <p><input class="theme-password" type="password" name="password" id="password"></p>

                <!-- We will redirect here when logged in -->
                <input type="hidden" name="redirect_url" value="/">

                <!-- submit button -->
                <p><input class="theme-button" type="submit" value="Login"></p>
            </form>
        </div>
        <div class="fourcol last"></div>
    </div>

@endsection