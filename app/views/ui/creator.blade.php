<div id="creator-dropdown" class="fl-dropdown regular fl-dropdown-tip">
	<ul class="fl-dropdown-menu">
	  <li><a id="creator_text" data-element-type="div" class="icon-align-left" href="#"> Text</a></li>
	  <li><a id="creator_image" data-element-type="figure" class="icon-picture" href="#"> Single Photo</a></li>
	  <li><a id="creator_galleryimage" data-element-type="galleryimage" class="icon-picture" href="#"> Gallery Image</a></li>
	  <li><a id="creator_collage" data-element-type="collage" class="icon-collage" href="#"> Photo Collage</a></li>
	  <li><a id="creator_video" data-element-type="youtube" class="icon-video" href="#"> Video</a></li>
	  <li><a id="creator_galleryvideo" data-element-type="galleryvideo" class="icon-play-circled" href="#"> Gallery Video</a></li>
	  <li><a id="creator_navigation" data-element-type="navigation" class="icon-menu" href="#"> Navigation</a></li>
	</ul>
</div>
