<div id="fl-control-bar" class="fl-control-bar">
	<div class="container">

		<div class="row">
			<div class="fivecol">
	            <p class="fl-menu clearfix">
	            	<a href="{{ URL::to('logout') }}" class="fl-tiny-button icon-lock">Logout</a>
	            	<a href="#" data-dropdown="#dropdown-1" id="hello" class="fl-tiny-button icon-wrench">Tools</a>
	            	<a href="{{ URL::to('/sitemap') }}" id="hello" class="fl-tiny-button icon-sitemap">Sitemap</a>
	            </p>

			</div>

			<div class="sevencol last sub-panel" style="text-align: right;">
				<span id="fl-publishing" class='fl-publishing' @if($edits_exist)style="display: block;"@endif>

					<form action="/elements" method="POST">
						<input type="submit" class="fl-publish fl-button" value="Publish" />
						<input type="hidden" name="_method" value="PUT" />
					</form>

					<form action="/elements" method="POST">
						<input type="submit" class="fl-undoall fl-button" value="Undo All" />
						<input type="hidden" name="_method" value="DELETE" />
					</form>
				</span>

				<p id="fl-notification" class='fl-notification' @if ($edits_exist || Session::has('notification'))style="display: block;"@endif>
					
					@if (Session::has('notification'))
						{{ Session::get('notification') }}
					@else
						All changes saved.
					@endif
				</p>
			</div>


		</div>

		</div>
</div>

<div class="fl-top-spacing">
&nbsp;
</div>