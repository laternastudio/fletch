<div id="matte" class="fl-panel-matte">
</div>
<div id="settings_panel" class="fl-panel">

	<form id="settings_form" action="" method="POST">
	<input type="hidden" id="settings_form_method" name="_method" value="PUT" />

	<div class="row">     
		<div class="fl-panel-title" class="twelvecol last">
			<h2 id="title"></h2>
			<p style="color: red; margin-bottom: 2em; max-width: 40em;" id="alert" class="alert"></p>
		</div>	
	</div>
	<div id="content" class="row">           	
	</div>
	<br style="clear: both;" />
	<div style="margin-top: 2em;" class="row">     
		<div class="twelvecol last" style="text-align: center;">
			<p style="vertical-align: middle; text-align: right; width: 25%; margin-right: 1em; font-size: 0.85em; display: inline-block;">Changes you make in this control panel will go into effect on the live site immediately.</p>
			<p style="vertical-align: middle; display: inline-block;">
				<input type="submit" class="fl-button" id="settings_save" name="settings_save" value="Save Changes" />
            	<a href="#" id="settings_cancel" class="fl-button">Cancel</a>
            </p>
		</div>
		</form>
	</div>
</div>