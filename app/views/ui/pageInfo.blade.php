<div id="global_page_id" hidden>{{ $page->id }}</div>
<div id="global_page_title" hidden>{{ HTML::entities($page->title) }}</div>
<div id="global_page_long_title" hidden>{{ HTML::entities($page->long_title) }}</div>
<div id="global_page_description" hidden>{{ HTML::entities($page->description) }}</div>
<div id="global_page_route" hidden>{{ HTML::entities($page->route) }}</div>
<div id="global_page_layout" hidden>{{ HTML::entities($page->layout) }}</div>
<div id="global_page_disabled" hidden>{{ HTML::entities($page->disabled) }}</div>
<div id="global_page_visible" hidden>{{ HTML::entities($page->visible) }}</div>
