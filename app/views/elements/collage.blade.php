{{-- View for the IMAGE COLLAGE element --}}
{{-- This view is used for all IMAGE COLLAGE elements within the Fletch CMS --}}

{{-- $content[0]->data   is the url of the first image --}}
{{-- $content[0]->id     is the database row id of the first image URL --}}
{{-- $content[1]->data   is the url of the second image --}}
{{-- $content[1]->id     is the database row id of the second image URL --}}
{{-- $content[2]->data   is the url of the third image --}}
{{-- $content[2]->id     is the database row id of the third image URL --}}
{{-- $content[3]->data   is the text for the caption --}}
{{-- $content[3]->id     is the database id for the caption text --}}
{{-- $element            contains all element data including primarily ->id --}}

<figure class="collage">

        {{-- First image (half) --}}
        <div class="half" data-aspect-ratio="1.5" data-size="small">
                <img src="{{URL::to('upload/img') }}/{{ $content[0]->data }}" id="fletching_img_{{ $element->id }}_{{ $content[0]->id }}" class="editableImage" data-url-id="{{ $content[0]->id }}" /> 
        </div>  

        {{-- Second image (half) --}}
        <div class="half" data-aspect-ratio="1.5" data-size="small">
                <img src="{{ URL::to('upload/img') }}/{{ $content[1]->data }}" id="fletching_img_{{ $element->id }}_{{ $content[1]->id }}" class="editableImage" data-url-id="{{ $content[1]->id }}" /> 
        </div>  

        {{-- Third image (full) --}}
        <div class="full" data-size="medium">
                <img src="{{ URL::to('upload/img') }}/{{ $content[2]->data }}" id="fletching_img_{{ $element->id }}_{{ $content[2]->id }}" class="editableImage" data-url-id="{{ $content[2]->id }}" /> 
        </div>  

        {{-- The caption for the image collage --}}
        @if (Auth::check() === true)
                {{-- Make the caption editable if the user is logged in --}}
                <p id="fletching_{{ $element->id }}" contenteditable="true" class="fl-editable" data-content-id="{{ $content[3]->id }}">
        @else
                {{-- Otherwise, the caption should not be editable --}}
                <p id="fletching_{{ $element->id }}">
        @endif
                {{-- Display the textual content inside the caption --}}
            {{ $content[3]->data }}
        </p>

</figure>