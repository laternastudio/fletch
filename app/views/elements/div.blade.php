{{-- View for the DIV element --}}
{{-- This view is used for all div elements within the Fletch CMS --}}

@if (Auth::check() === true)
	{{-- Make the DIV editable if the user is logged in --}}
	<div id="fletching_{{ $element->id }}" contenteditable="true" class="fl-editable" data-content-id="{{ $content[0]->id }}">
@else
	{{-- Otherwise, the DIV should not be editable --}}
	<div id="fletching_{{ $element->id }}">
@endif
	{{-- Display the textual content inside the DIV --}}
    {{ $content[0]->data }}
</div>