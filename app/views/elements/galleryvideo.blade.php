{{-- View for the GALLERYVIDEO element --}}
{{-- This view is used for all GALLERYVIDEO elements within the Fletch CMS --}}

{{-- $content[0]->data   is the url of the image --}}
{{-- $content[0]->id     is the database id of the URL row --}}
{{-- $content[1]->data   is the text for the caption --}}
{{-- $content[1]->id     is the database id for the caption text --}}
{{-- $element            contains all element data including primarily ->id --}}


@if (Auth::check() === true)

<figure class="gallery">        

	{{ Render::youtubeThumbnail($content[0]->data) }}

</figure>

@else

<figure class="gallery{{ $classes }}">        
    <a href="http://www.youtube.com/watch?v={{ $content[0]->data }}" class="gallery-video mfp-iframe" title="#">
    	<i></i>
		{{ Render::youtubeThumbnail($content[0]->data) }}
    </a>
</figure>@endif