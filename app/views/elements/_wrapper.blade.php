{{-- View for the wrapper --}}
{{-- This view is used to encapsulate elements when they are --}}
{{-- inserted via AJAX. --}}

<li id="element_{{ $element_id }}" class="{{ $classes }}" data-element-id="{{ $element_id }}" >
	{{ $element_html }}
 	<div class="fl-element-controls">
 		<span class="move icon-move handle" alt="Move" title="Move"></span>
 		<span class="edit icon-pencil" alt="Edit" title="Edit"></span>
 		<span class="delete icon-trash" alt="Delete" title="Delete"></span>
	</div>
</li>