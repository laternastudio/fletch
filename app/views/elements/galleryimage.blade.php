{{-- View for the GALLERYIMAGE element --}}
{{-- This view is used for all GALLERYIMAGE elements within the Fletch CMS --}}

{{-- $content[0]->data   is the url of the image --}}
{{-- $content[0]->id     is the database id of the URL row --}}
{{-- $content[1]->data   is the text for the caption --}}
{{-- $content[1]->id     is the database id for the caption text --}}
{{-- $element            contains all element data including primarily ->id --}}


@if (Auth::check() === true)
<figure class="gallery" data-lightbox="true">        
    <i></i>
	@if (Str::endsWith($content[0]->data, 'jpg'))
    	<img src="{{ URL::to('upload/img') }}/{{ Str::limit($content[0]->data, Str::length($content[0]->data) - 4, '') }}_thumb.jpg" id="fletching_img_{{ $element->id }}" class="editableImage" data-url-id="{{ $content[0]->id }}" />
    @elseif (Str::endsWith($content[0]->data, 'png'))
    	<img src="{{ URL::to('upload/img') }}/{{ Str::limit($content[0]->data, Str::length($content[0]->data) - 4, '') }}_thumb.png" id="fletching_img_{{ $element->id }}" class="editableImage" data-url-id="{{ $content[0]->id }}" />
    @endif

	<input type="hidden" class="option" data-heading="Image Caption" data-button-text="Save" data-icon="" data-message="Enter a caption for your image:" value="{{ $content[1]->data }}" data-element-id="{{ $element->id }}" data-content-id="{{ $content[1]->id }}" />

</figure>

@else

<figure class="gallery{{ $classes }}">        
    <a href="{{ URL::to('upload/img') }}/{{ $content[0]->data }}" class="gallery-image mfp-image" title="{{ $content[1]->data }}">
		<i></i>
		@if (Str::endsWith($content[0]->data, 'jpg'))
	    	<img src="{{ URL::to('upload/img') }}/{{ Str::limit($content[0]->data, Str::length($content[0]->data) - 4, '') }}_thumb.jpg" id="fletching_img_{{ $element->id }}" class="editableImage" data-url-id="{{ $content[0]->id }}" />
	    @elseif (Str::endsWith($content[0]->data, 'png'))
	    	<img src="{{ URL::to('upload/img') }}/{{ Str::limit($content[0]->data, Str::length($content[0]->data) - 4, '') }}_thumb.png" id="fletching_img_{{ $element->id }}" class="editableImage" data-url-id="{{ $content[0]->id }}" />
	    @endif
    </a>
</figure>@endif