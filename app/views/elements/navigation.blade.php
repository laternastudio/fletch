{{-- View for the SUBNAV element --}}
{{-- This view is used for all SUBNAV elements within the Fletch CMS --}}

@if (Auth::check() === true)
	{{-- Make the SUBNAV editable if the user is logged in --}}
	<nav class="sub-navigation">
		@if(Render::navigation(rtrim(ltrim($content[0]->data,'/'),'/').'/') === '')
			{{-- If there are no sub-pages, display fallback text --}}
			<p>There are no sub-pages to display as navigation.</p>
		@else
			{{-- Otherwise, display the links --}}
			<ul>
				{{ Render::navigation(rtrim(ltrim($content[0]->data,'/'),'/').'/') }}
			</ul>
		@endif

		<input type="hidden" class="option" data-heading="Navigation Category" data-button-text="Save" data-icon="" data-message="Enter the address to display navigation for:" value="{{ $content[0]->data }}" data-element-id="{{ $element->id }}" data-content-id="{{ $content[0]->id }}" />
	</nav>
@else
	{{-- Otherwise, the SUBNAV should not be editable --}}
	<nav class="sub-navigation">
		@if(Render::navigation(rtrim(ltrim($content[0]->data,'/'),'/').'/') === '')
			{{-- If there are no sub-pages, display fallback text --}}
			<p>There are no sub-pages to display as navigation.</p>
		@else
			{{-- Otherwise, display the links --}}
			<ul>
				{{ Render::navigation(rtrim(ltrim($content[0]->data,'/'),'/').'/') }}
			</ul>
		@endif
	</nav>
@endif
