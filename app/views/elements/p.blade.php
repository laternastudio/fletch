{{-- View for the P element --}}
{{-- This view is used for all P elements within the Fletch CMS --}}

@if (Auth::check() === true)
	{{-- Make the P editable if the user is logged in --}}
	<p id="fletching_{{ $element->id }}" contenteditable="true" class="fl-editable" data-content-id="{{ $content[0]->id }}">
@else
	{{-- Otherwise, the P should not be editable --}}
	<p id="fletching_{{ $element->id }}">
@endif
	{{-- Display the textual content inside the P --}}
    {{ $content[0]->data }}
</p>