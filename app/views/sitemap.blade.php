@extends('layouts.default')

@section('title')
	Sitemap
@endsection

@section('content')
	<h1 style="text-align: center;">Sitemap</h1>

	<div class="row">
		<div class="fourcol" >
		</div>

		<div class="fourcol sitemap" >
			{{ Render::sitemap() }}
		</div>

		<div class="fourcol last" >
		</div>
	</div>
@endsection