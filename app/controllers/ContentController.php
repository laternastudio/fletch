<?php

use Fletch\Entities\User as User;
use Fletch\Entities\Element as Element;
use Fletch\Entities\Content as Content;
use Fletch\Entities\ContentDraft as ContentDraft;
use Fletch\Entities\ElementDraft as ElementDraft;
use Fletch\Entities\Setting as Setting;
use Fletch\Entities\Page as Page;

class ContentController extends BaseController {

	public $restful = true;

	/**
	 * Content creation is all handled as part of element creation.
	 * 
	 * When creating an element (via the ElementController) all
	 * content rows are also created. Because of this, there is no
	 * true 'create' method for the ContentController. This method is
	 * shown here merely to make it clear that its omission is
	 * intentional, and to explain the reason.
	 * 
	 * @see ElementController.php
	 * 
	 */
    public function post_create() {
    }


    /**
    * Update a single piece of content for an element.
    * 
    * Each element is made up of one or more pieces of content. This
    * method handles updating one of those pieces of content.
    * 
    * @param  string  $element_id   the database ID of the element
    * @param  string  $content_id   the database ID of the content
    * 
    * @uses   string  $_GET['type']            determines the content to be saved ('text' or 'image')
    * @uses   string  $_POST['data']           the actual data to save
    * @uses   string  $_POST['image']          the region in which the element should be rendered
    * @uses   string  $_POST['aspect_ratio']   the image's aspect ratio (width / height)
    * @uses   string  $_POST['size']           the image's size
    * 
    */
    public function put_update($element_id, $content_id) {
        
        // Determine whether this update request targets text or an image
		$update_type = Input::get('type');
        
		/*
		 * Perform the text update operation
		 */ 
		if ($update_type == 'text')
		{
            $data = Utility::cleanStr(Input::get('data'));

            // Delete any existing draft rows
            ContentDraft::where('content_id', '=', $content_id)->delete();

            // Create a draft row
            $new_content_draft = new ContentDraft;
            $new_content_draft->content_id = $content_id;
            $new_content_draft->data = $data;
            $new_content_draft->save();
    	}
        
		/**
		 * Perform the image update operation
		 */
		if($update_type == 'image') {
            
			$input = Input::all();
            
            $image        = Input::file('image');
			$aspect_ratio = Input::get('aspect_ratio', 0);
			$size         = Input::get('size', 'any');
            
            // Fix up the filename
			$filename = $input['image']->getClientOriginalName();
            $filename = FileTools::slugifyFilename($filename);
            $filename = FileTools::uniqueFilename($filename);

            // Initialize the image for processing
            $image = PHPImageWorkshop\ImageWorkshop::initFromPath($input['image']->getRealPath());

            // Run the image through a few preparatory procedures
            $image = FileTools::limitResolution($image);
            $image = ($aspect_ratio > 0) ? FileTools::cropToAspectRatio($image, $aspect_ratio) : $image;
            $image = FileTools::resizeToPreset($image, $size);

            // Save out the final image.
            $image->save('upload/img/', $filename, false, false, 85);

            // Create the thumbnail
            $thumbnail = $image;
            $thumbnail = FileTools::cropToAspectRatio($thumbnail, 1.3);
            $thumbnail->resizeInPixel(350, null, true);
            $thumbnail_filename = FileTools::appendToFilename($filename, '_thumb');
            $thumbnail->save('upload/img/', $thumbnail_filename, false, false, 85);

            // Delete any existing draft rows (and draft row images)
            $draft_rows = ContentDraft::where('content_id', '=', $content_id)->get();

            foreach($draft_rows as $draft_row) {
                $draft_filename           = $draft_row->data;
                $draft_thumbnail_filename = FileTools::appendToFilename($draft_filename, '_thumb');

                // Delete the image files
                File::delete('upload/img/'.$draft_filename);
                File::delete('upload/img/'.$draft_thumbnail_filename);

                // Delete the database record
                $draft_row->delete();
            }

            // Create a new draft row
            $content_draft = new ContentDraft;
            $content_draft->content_id = $content_id;
            $content_draft->data = $filename;
            $content_draft->save();

            // Prep the data for the json response
			$url = URL::to('upload/img/'.$filename);
            $data = array('url' => $url);

		}
        
        // Return the successful response along with any data that should
        // be returned, such as an image url.
	    return Response::json($data);
    }
    
    
	/**
	 * Content deletion is all handled as part of element deletion.
	 * 
	 * When deleting an element (via the ElementController) all
	 * content rows are also deleted. Because of this, there is no
	 * true 'destroy' method for the ContentController. This method is
	 * shown here merely to make it clear that it's omission is
	 * intentional, and to explain the reason.
	 * 
	 * @see ElementController.php
	 * 
	 */
	public function delete_destroy() {
    }

}