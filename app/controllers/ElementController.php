<?php

use Fletch\Entities\User as User;
use Fletch\Entities\Element as Element;
use Fletch\Entities\Content as Content;
use Fletch\Entities\ContentDraft as ContentDraft;
use Fletch\Entities\ElementDraft as ElementDraft;
use Fletch\Entities\Setting as Setting;
use Fletch\Entities\Page as Page;

class ElementController extends BaseController {

	public $restful = true;


	/**
	 * Create a new element.
	 * 
	 * This method contains the functionality for creating new elements.
	 * What these elements are/do is based entirely on the actual element XML
	 * files and views (which will be reworked soon).
	 * 
	 * @uses  string  $_POST['page]        The database id of the page.
	 * @uses  string  $_POST['region']     The region to which the element should belong
	 * @uses  string  $_POST['type']       The type of element that should be created
	 * @uses  string  $_POST['data']       Any data needed in order to create the element (such as a youtube url)
	 */
	public function post_create() {

		$page  = Input::get('page');
		$region  = Input::get('region');
		$type  = Input::get('type');
		$data = Input::get('data');

		/**
		 * Find out what the new element's position should be.
		 */
        
        // First we'll get the highest draft position in the region
        $max_draft_position = ElementDraft::
            where('region', '=', $region)
            ->whereIn('element_id', function($query) use ($page)
            {
                $query->select('id')
                    ->from('elements')
                    ->where('page_id', $page);
            })
            ->max('position');
        
        // Now we'll get the highest saved element position in the region
        $max_position = Element::
            where('page_id', $page)
            ->where('region', '=', $region)
            ->max('position');
        
        // Our new element will have a position that is one greater
        // than the higher of the two ($max_draft_position and $max_position)
		if(isset($max_draft_position) || isset($max_position)) {
            $position = max(array($max_draft_position, $max_position)) + 1; 
        }
        // If there were elements in the region, position is zero.
		else {
            $position = 0; 
        }
           

		/**
		 * Create the element.
		 */

        // Load up the page
        $page = Page::where('id', '=', $page)->first();

        // Create the element
        $new_element = new Element;
        $new_element->page_id  = $page->id;
        $new_element->region   = $region;
        $new_element->position = $position;
        $new_element->type     = $type;
        $new_element->save();

        // Grab the id of the element we just inserted
        $element_id = $new_element->id;

        // Load up the XML file
        $element_values = Utility::loadFromXML('../app/elements/'.$type.'.xml');

        // If data was posted in, use it. Otherwise use default.
        // Note that data would have to be posted in via a $data array and its
        // index would have to match the XML content index that it was intended
        // to overwrite.
        for($i = 0; $i < count($element_values->content); $i++) {
            $element_values->content[$i] = (isset($data[$i])) ? $data[$i] : $element_values->content[$i];        	
        }

        // Create a $content array to be passed into the view
        // This will be passed into the blade view for the rendering
        // It's labeled $all_content because the element is likely
        // made up of multiple pieces of content.
        $all_content = array();

        // Create the content based on the XML
        foreach($element_values->content as $content) {

            $new_content = new Content;
            $new_content->element_id = $element_id;
            $new_content->data = $content;
            $new_content->save();

            // Save the newly created object to the $content array.
            array_push($all_content, $new_content);   	
        }

        // Load up special classes for elements
        $classes = "";

        foreach ($element_values->{'class'} as $class) {
            $classes .= " ".$class;
        }

        // Render the html based on its corresponding view
        // This will be returned and displayed via jquery
        $element_html = View::make('elements.'.$new_element->type, array('element' => $new_element, 'content' => $all_content, 'page' => $page));
        $finished_html = View::make('elements._wrapper', array('element_id' => $new_element->id, 'element_html' => $element_html, 'classes' => $classes));

        // Prepare the return variables
        $return_array = array(
            'html' => (string)$finished_html,
            'element_id' => $new_element->id
        );				

		return Response::json($return_array);
	}


    /**
    * Update the position of an element or flag it for removal.
    * 
    * This function performs one of two distinct actions. The first is to save 
    * the position (region, position) of an element. The second is to flag an 
    * element for deletion. You can specify which of these operations should be 
    * performed by using a $_GET['type] variable that includes either the
    * the value 'position' or 'destroy. Note that all updates are saved
    * merely as drafts, and do not go live until the changes are published.
    * 
    * @param  string  $element_id            the database ID of the element
    * 
    * @uses   string  $_GET['type']          determines operation to be executed ('content', 'position', or '')
    * @uses   string  $_POST['region']       the region in which the element should be rendered
    * @uses   string  $_POST['position']     the element's position within the region
    * 
    */
	public function put_update($element_id) {
        
        // Determine whether this update request targets content, position, or destruction
		$update_type = Input::get('type');

		/*
		 * Perform the position update operation
		 */ 
		if ($update_type == 'position')
		{
            $region = Input::get('region');
            $position = Input::get('position');

			// Save the position for the specified element
            $element = Element::find($element_id);

            // Delete any existing draft rows
            ElementDraft::where('element_id', '=', $element_id)->delete();

            // Create a draft row
            $new_element_draft = new ElementDraft;
            $new_element_draft->element_id = $element_id;
            $new_element_draft->region = $region;
            $new_element_draft->position = $position;
            $new_element_draft->deleted = '0';
            $new_element_draft->save();

		}

		/*
		 * Perform the destruction update operation. Remember that this merely flags
		 * an element for deletion. It doesn't actually get deleted until changes are published.
		 */ 
		if ($update_type == 'destroy')
		{
            
            // Check for an existing draft row
            $draft = ElementDraft::where('element_id', '=', $element_id)->first();

            
            if(!is_null($draft)) {
                // Update the draft
                $draft->deleted = '1';
                $draft->save();
            }
            else {
                // Grab the neccesary element information
                $element  = Element::find($element_id);
                $region   = $element->region;
                $position = $element->position;
                
                // Create the draft
                $draft = new ElementDraft;
                $draft->element_id = $element_id;
                $draft->region = $region;
                $draft->position = $position;
                $draft->deleted = '1';
                $draft->save();
            }

		}

       	// Let our application know that the operation was a success.
        return Response::make(NULL, 200);
	}
   
    
    /**
     * Publish all of the changes.
     * 
     * Publishing all of the changes consists of a few distinct procedures:
     * 1. Delete all elements marked for deletion, including relations and images
     * 2. Publish all content drafts
     * 3. Publish all element drafts
     * 4. Mark all elements as confirmed
     */
	public function put_publish() {
        
        // The filename of Fletch's placeholder image. We'll be sure not to delete it.
        $app_placeholder_image = 'icon-img.png';

        
        /*
         * 1. Delete all elements marked for deletion, including relations and images
         */
        
        // Select all content and content_drafts rows that have to do with images
        $image_content = Content::with('drafts')
            ->whereIn('element_id', function($query)
                      {
                          $query->select('element_id')
                              ->from('element_drafts')
                              ->where('deleted', '=', '1');
                      })
            ->where(function($query)
                    {
                        $query->where('data', 'like', '%.png')
                            ->orWhere('data', 'like', '%.gif')
                            ->orWhere('data', 'like', '%.jpg')
                            ->orWhere('data', 'like', '%.jpeg');
                    })->get();

        // Delete the images that this query retrieved.
        foreach($image_content as $image) {

            // Delete the saved image files
            $filename          = $image->data;
            $thumbnail_filename = FileTools::appendToFilename($filename, '_thumb');

            File::delete('upload/img/'.$filename);
            File::delete('upload/img/'.$thumbnail_filename);

            // Delete the draft image files
            if(isset($image->drafts)) {
                $draft_filename           = $image->drafts->data;
                $draft_thumbnail_filename = FileTools::appendToFilename($draft_filename, '_thumb');

                File::delete('upload/img/'.$draft_filename);
                File::delete('upload/img/'.$draft_thumbnail_filename);
            }
        }  
        
        // Delete all the element rows including all of the content as well as drafts
        $elements_to_delete = Element::
            with('drafts')
            ->with('content')
            ->with('content.drafts')
            ->whereIn('id', function($query)
                      {
                          $query->select('element_id')
                              ->from('element_drafts')
                              ->where('deleted', '=', '1');
                      })
            ->delete();
        

        /*
         * 2. Publish all content drafts
         */
        
        $content_with_drafts = Content::with('drafts')
            ->whereIn('id', function($query)
            {
                $query->select('content_id')
                    ->from('content_drafts');
            })->get();
        
        
        // Cycle through every element_drafts
        foreach($content_with_drafts as $content) {
            
            // Grab the path parts so that we can test if the string is a filename
            $path_parts = pathinfo($content->data);
            
            // If it's an image, delete the original image
            // (unless it's the application placeholder image)
            if (isset($path_parts['extension']) && $content->data != $app_placeholder_image) {
                
                $filename = $content->data;
                $thumbnail_filename = FileTools::appendToFilename($filename, '_thumb');

                File::delete('public_html/upload/img/'.$filename);
                File::delete('public_html/upload/img/'.$thumbnail_filename);
            }
            
            // Update the content row
            $content->data = $content->drafts->data;
            $content->save();
        }
        
         // Delete all of the content drafts
        ContentDraft::truncate();       
        
        
        /*
         * 3. Publish all element drafts
         */
        
        // Copy over all of the draft element values to the actual element rows
        DB::table('elements as e')
            ->join('element_drafts as d', 'd.element_id', '=', 'e.id')
            ->update(['e.region' => DB::raw('d.region'), 'e.position' => DB::raw('d.position')]);
        
        // Delete all of the element drafts
        ElementDraft::truncate();
        

                      
        /*
         * 4. Mark all elements as confirmed
         */
        
        $confirmed_rows = DB::table('elements')->update(array('confirmed' => 1));
	    

	    return Redirect::back()->with('notification', 'Draft successfully published!');
	}

    
    /**
     * Undo all changes since the last publish.
     * 
     * This function removes any content or element changes that
     * have been made since the last published version.
     */
	public function delete_undo() {
        

        // Delete all of the content_draft images
        $images = ContentDraft::where('data', 'like', '%.png')
            ->orWhere('data', 'like', '%.gif')
            ->orWhere('data', 'like', '%.jpg')
            ->orWhere('data', 'like', '%.jpeg')
            ->get();

        foreach($images as $image) {
            $filename = $image->data;
            $thumb_filename = FileTools::appendToFilename($filename, '_thumb');

            File::delete('public_html/upload/img/'.$filename);
            File::delete('public_html/upload/img/'.$thumb_filename);
        }


        // Delete all of the drafts
        ContentDraft::truncate();
        ElementDraft::truncate();

        // Delete unconfirmed elements including content rows
        Element::with('content')->where('confirmed', '=', '0')->delete();

        return Redirect::back()->with('notification', 'Changes successfully undone!');

	}
}