<?php

use Fletch\Entities\User as User;
use Fletch\Entities\Element as Element;
use Fletch\Entities\Content as Content;
use Fletch\Entities\ContentDraft as ContentDraft;
use Fletch\Entities\ElementDraft as ElementDraft;
use Fletch\Entities\Setting as Setting;
use Fletch\Entities\Page as Page;

class SettingController extends BaseController {

	// all of these actions should only be available if logged in

	public $restful = true;

	public function get_show() {
		// Nothing
	}

	public function post_create() {
		$type = Input::get('type');

		switch($type) {
			case 'slideshow' :


				$values = Input::all();

				$delete = array();

				foreach($values as $key => $value) {
					// Build the array of images to delete
					$broken_key = explode('_', $key);
					if($broken_key[0] == 'delete') {
						array_push($delete, $broken_key[1]);
					}
				}

				try {
					// Delete the images to delete
					foreach($delete as $item) {
						$image_to_delete = Setting::find($item);
						$filename = $image_to_delete->value;
						$thumbname = substr($filename, 0, -4).'_thumb.jpg';
						File::delete('public_html/upload/img/'.$filename);
						File::delete('public_html/upload/img/'.$thumbname);

						Setting::find($item)->delete();
					}	
				}
				catch(Exception $e) {
					return Redirect::back()->with('notification', 'Image delete could not be performed.');
				}

			if(!empty($values['image']['name'])) {
				try {

					// Generate the filename
					$uid = substr(sha1(strtotime("now")), 0, 8);
					$slugified_name = Render::slugify($values['image']['name']);
					$remove_extension = explode('-', $slugified_name);
					array_pop($remove_extension);
					$slugified_name = implode("-", $remove_extension);
					$filename = $uid.'_'.$slugified_name.'.jpg';
					$thumbname = $uid.'_'.$slugified_name.'_thumb.jpg';

					// Upload the file
					$allowed =  array('gif', 'png', 'jpg', 'jpeg');
					$ext = strtolower(pathinfo($values['image']['name'], PATHINFO_EXTENSION));
					if(!in_array($ext, $allowed) ) {
					    return Redirect::back()->with('notification', 'Error. Only .gif, .png, and .jpg file types are supported.');
					}

					// Initialize the image for processing
					$image = PHPImageWorkshop\ImageWorkshop::initFromPath($values['image']['tmp_name']);	
					
					// Failsafe in case the image is really big
					// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
					if(max($image->getHeight(), $image->getHeight()) > 2000 ) {

						// Resize based on the larger attribute (width or height)
						if($image->getWidth() > $image->getHeight()) {
							$image->resizeInPixel(2000, null, true);
						}
						else {
							$image->resizeInPixel(null, 2000, true);
						}

						$image->save('public_html/upload/img/', $filename, false, false, 100);
						$image = PHPImageWorkshop\ImageWorkshop::initFromPath('public_html/upload/img/'.$filename);	
					}
					// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

					$aspect_ratio = 3.2;
				    $crop_height = min($image->getWidth() / $aspect_ratio, $image->getHeight());
				    $crop_width = $crop_height * $aspect_ratio;
				    $image->cropInPixel($crop_width, $crop_height, 0, 0, 'MM');

					// Resize the image to the specified size
					$image->resizeInPixel(1600, 500, true);
					$image->save('public_html/upload/img/', $filename, false, false, 75);

					// Create the thumbnail
					$image->resizeInPercent(15, 15, true);
					$image->save('public_html/upload/img/', $thumbname, false, false, 75);

				    // Create a setting
				    $new_setting = new Setting;
				    $new_setting->name = 'slide';
				    $new_setting->value = $filename;
				    $new_setting->save();

				}
				catch(Exception $e) {
					Log::info($e);
					return Redirect::back()->with('notification', 'Image could not be added to the slideshow.');
				}				
			}



			return Redirect::back()->with('notification', 'Slideshow changes saved successfully!');

			break;
		}

	}

	public function put_update() {

		$type = Input::get('type');

		// Update social media
		// -----------------------
		switch($type) {
			case 'socialmedia' :

				$urls = Input::all();

				try {
				    // Create a draft row
			        foreach($urls as $platform=>$url) {

			        	Setting::where('name', '=', $platform)->delete();

			        	if($platform != 'settings_save' && $platform != 'type' && $url != '') {

						    $new_setting = new Setting;
						    $new_setting->name = $platform;
						    $new_setting->value = $url;
						    $new_setting->save();	        		
			        	}
			        }
		            return Redirect::back()->with('notification', 'Social media links have been successfully updated!');
				}
				catch(Exception $e) {
					Log::info($e);
		            return Redirect::back()->with('notification', 'Social media links could not be updated.');
				}
			break;

			case 'global' :

				$old_password          = Input::get('old_password');
				$new_password          = Input::get('new_password');
				$confirm_new_password  = Input::get('confirm_new_password');
				$emaillist             = Input::get('emaillist');
				$contact_form_email    = Input::get('contact_form_email');
				Log::password_check('NEW: '.$new_password.' CONFIRM: '.$confirm_new_password);
				
				try {
					// *******************
					// Update the Password
					if ($new_password != '' && $confirm_new_password !='') {

						// Hash the passwords
						$new_password = Hash::make($new_password);

						// Grab current user
						$user = User::where('username', '=', 'admin')->first();

						// Change the password
						if (Hash::check($old_password, $user->password)
							&& Hash::check($confirm_new_password, $new_password))
						{
							$user->password = $new_password;
							$user->save();
						}
						else {
							return Redirect::back()->with('notification', 'Password update failed. Make sure your passwords match!');
						}				
					}					
				}
				catch (Exception $e) {
					Log::password_change($e);
					return Redirect::back()->with('notification', 'Password update failed.');
				}


				// *********************
				// Update the E-mail List
				if(!is_null($emaillist)) {
					try {
						$db_emaillist = Setting::where('name', '=', 'emaillist')->first();
						$db_emaillist->value = $emaillist;
						$db_emaillist->save();
					}
					catch (Exception $e) {
						return Redirect::back()->with('notification', 'E-mail list update failed.');
					}					
				}

				try {
					// ******************************
					// Update the Contact Form E-mail
					$db_contact_form_email = Setting::where('name', '=', 'contact_form_email')->first();
					$db_contact_form_email->value = $contact_form_email;
					$db_contact_form_email->save();
				}
				catch (Exception $e) {
					return Redirect::back()->with('notification', 'Contact form e-mail update failed.');
				}

				return Redirect::back()->with('notification', 'Site settings successfully updated!');
			break;

			case 'emaillist' :

				// Grab e-mail
				$email = Input::get('email');

				// Validate E-mail
				if(filter_var($email, FILTER_VALIDATE_EMAIL)) {

					$email_list = Setting::where('name', '=', 'emaillist')->first();

					// Check if it already exists
					if (strpos($email_list->value, $email) === false) {
						// Save E-mail
						try {
							$email_list->value .= ($email_list->value == '') ? $email : ', '.$email;
							$email_list->save();
							$return_array = array('success' => 'true');

							// Send a notification to the Glow Spa Administrator
							$formatted_message  = 'Hi, ';
							$formatted_message .= "There's a new member of the glowspa.org e-mail list!";
							$formatted_message .= "\n\n";
							$formatted_message .= "Their e-mail address is ".$email.".\n\n";
							$formatted_message .= "Regards,\n";
							$formatted_message .= 'Glowspa.org';

							// Load the send e-mail address
							$email_row = Setting::where('name', '=', 'contact_form_email')->first();
							$email = $email_row->value;

							// If you do not want to auto-load the bundle, you can use this
							Bundle::start('swiftmailer');

							// Get the Swift Mailer instance
							$mailer = IoC::resolve('mailer');

							// Construct the message
							$message = Swift_Message::newInstance('Message From Website')
							    ->setFrom(array('noreply@glowspa.org' => 'Glowspa.org'))
							    ->setTo(array($email => 'Glow Skincare and Spa'))
							    ->setSubject('New Glowspa.org Subscriber')
							    ->setBody($formatted_message,'text/plain');

							// Send the email
							$mailer->send($message);
						}
						catch (Exception $e) {
							Log::error($e);
							$return_array = array('success' => 'false');
						}
					}
					else {
						Log::info('The e-mail address already existed!');
						$return_array = array('success' => 'true');
					}
				}
				else {
					$return_array = array('success' => 'false');					
				}


				return Response::json($return_array);
			break;
		}

	}

	public function delete_destroy() {

	}

}

?>