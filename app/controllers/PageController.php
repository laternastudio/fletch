<?php

use Fletch\Entities\User as User;
use Fletch\Entities\Element as Element;
use Fletch\Entities\Content as Content;
use Fletch\Entities\ContentDraft as ContentDraft;
use Fletch\Entities\ElementDraft as ElementDraft;
use Fletch\Entities\Setting as Setting;
use Fletch\Entities\Page as Page;


class PageController extends BaseController {

	public $restful = true;

	/*
	|--------------------------------------------------------------------------
	| The Pageloader Controller
	|--------------------------------------------------------------------------
	|
	|	The pageloader controller catches all requests not specified in the
	|	written routes and sees if they exist as fletch pages. If they do
	|	they are loaded and rendered.
	*/
	public function get_load()
	{
		$page = Page::with(array(
            'elements',
            'elements.drafts',
            'elements.content',
            'elements.content.drafts'
        ))->where('route', '=', 'about-us')->first();

		echo "<pre>";
		print_r($page->elements);	
		echo "</pre>";

		// $route = Request::path();
		// $settings = Setting::all();
		// $routes = Page::load_routes();
		// $page = Page::load_by_route($route);

		// // If the page is still not found (i.e. there was no index page)
		// // throw a 404 error. Otherwise show the page.
		// if($page) {
		// 	$layout = 'sublayouts.'.$page->layout;

		// 	return View::make($layout)
		// 		->with('page', $page)
		// 		->with('reserved_routes', $routes)
		// 		->with('settings', $settings)
		// 		->with('path', $route);
		// }
		// else {
		// 	Log::info($route);
		// 	Log::info(print_r($page));
		// 	App::abort(404);
		// }
	}

	public function post_create()
	{

		// Get the variables that were PUT
		$title = Input::get('page_title');
		$route = Input::get('page_route');
		$description = Input::get('page_description');
		$layout = Input::get('layout');
		$acceptable_route = '/^[a-z0-9]+[a-z0-9\/\-\_]+[a-z0-9]$/i';
		$reserved_routes = Page::load_routes();
		$long_title = Input::get('page_long_title');
		$description = Input::get('page_description');

		// Validate the data
		$route_unique = in_array($route, $reserved_routes) ? false : true;
		$route_okay = preg_match($acceptable_route, $route);
		$title_okay = ($title != '');

		if ($route_okay && $title_okay && $route_unique) {
	        try {

				// Create the page
				$new_page = new Page;
				$new_page->route = $route;
				$new_page->title = $title;
				$new_page->long_title = $long_title;
				$new_page->description = $description;
				$new_page->layout = $layout;
				$new_page->save();

	        	return Redirect::to($route)->with('notification', 'Page has been created!');
	        }
	        catch (Exception $e) {
	            Log::info('There was an error creating the page.');
	            return Redirect::back()->with('notification', 'Page could not be created.');
	        }			
		}
		else {
	        return Redirect::back()->with('notification', 'Page could not be created.');			
		}			


	}

	public function put_update()
	{
		// Get the variables that were PUT
		$page_id = Input::get('page_id');
		$page_delete = strtolower(Input::get('page_delete'));

		if($page_delete == 'delete') {

    		$page = Page::find($page_id)->delete();

		    return Redirect::to('/')->with('notification', 'Page has been deleted successfully!');
		}
		else {
			$new_title = Input::get('page_title');
			$new_route = strtolower(Input::get('page_route'));
			$new_long_title = Input::get('page_long_title');
			$new_description = Input::get('page_description');
			$acceptable_route = '/^[a-z0-9]+[a-z0-9\/\-\_]+[a-z0-9]$/i';
			$reserved_routes = Page::load_routes();
			$new_layout = Input::get('layout');
			$new_visibility = (Input::get('page_visible') == '1') ? '1' : '0';

			echo $new_visibility;

		    try {
		        
		        $page = Page::find($page_id);

				// Validate the data
				$route_unique = (in_array($new_route, $reserved_routes) && $new_route != $page->route) ? false : true;
				$route_okay = preg_match($acceptable_route, $new_route);
				$title_okay = ($new_title != '');

				if ($route_okay && $title_okay && $route_unique) {

		        	$page->title = $new_title;
		        	$page->route = $new_route;
		        	$page->layout = $new_layout;
		        	$page->long_title = $new_long_title;
		        	$page->description = $new_description;
		        	$page->visible = $new_visibility;
		        	$page->save();

		        	return Redirect::to($new_route)->with('notification', 'Page changes have been saved!');
		        }
				else {
			        return Redirect::back()->with('notification', 'Page changes could not be saved.');			
				}
			}
	        catch (Exception $e) {
	            Log::info('There was an error saving the page.');
	            return Redirect::back()->with('notification', 'Page changes could not be saved.');
	        }	
		}
	}

	public function delete_destroy()
	{
		//
	}

}