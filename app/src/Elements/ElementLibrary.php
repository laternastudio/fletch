<?php namespace Fletch\Elements;

class ElementLibrary {

	/** @var  array */
	public $elements = [];


	/**
	 * Build the element library
	 *
	 * @param  array  an array of element objects
	 */
	public function __construct() {
	}

	/**
	 * Register all of the elements
	 *
	 * @param  object  an element object
	 */
	public function registerElement(\Fletch\Elements\ElementLibrary $element) {
		$this->elements[$element->slug] = $element;
	}

}