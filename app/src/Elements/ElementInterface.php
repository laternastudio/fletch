<?php namespace Fletch\Elements;

interface ElementInterface {

	/** @var string */
	$class;

	/** @var string */
	$type

	/** @var array */
	$content;

	/** @var string */
	$hidden;
	
}