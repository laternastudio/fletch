<?php namespace Fletch\Elements;

class Content {

	/** @var  string */
	public $id;

	/** @var  string */
	public $data;

	/** @var  string */
	public $created_at;

	/** @var  string */
	public $updated_at;
}


?>