<?php namespace Fletch\Pages;

interface RepositoryInterface {

	public function create(array $data);

	public function update($id, array $data);

	public function delete($id);
}