<?php namespace Fletch\Pages;

interface PageRepositoryInterface extends RepositoryInterface {

	/**
	 * 
	 */
	public function create(array $data)
	{

	}

	/**
	 * 
	 */
	public function update($id, array $data)
	{

	}

	/**
	 * 
	 */
	public function delete($id)
	{

	}

	/**
	 * 
	 */
	public function getPageByRoute($route)
	{

	}

	/**
	 * 
	 */
	public function getAllRoutes()
	{

	}
}