<?php
namespace Fletch\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

// application/models/elements.php
// The model for editable text or image elements within the CMS.

class Setting extends Eloquent {
	protected $table = 'settings';

	public static function load_as_array() {
		$db_settings = self::all();
		$settings = array();

		foreach($db_settings as $setting) {
			$settings[$setting->name] = $setting->value;
		}

		return $settings;
	}

}

?>