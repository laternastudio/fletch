<?php
namespace Fletch\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;
// application/models/element_draft.php
// The model for editable text or image elements within the CMS.

class ElementDraft extends Eloquent {
	protected $table = 'element_drafts';

	public function elements() {
		return $this->belongsTo('Fletch\Entities\Element');
	}
}

?>