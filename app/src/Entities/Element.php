<?php
namespace Fletch\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;
// application/models/elements.php
// The model for editable text or image elements within the CMS.

class Element extends Eloquent {

	protected $table = 'elements';

    public function delete()
    {
        // delete all related content
        foreach($this->content as $content)
        {
            $content->delete();
        }

        // delete all related drafts 
        $this->drafts()->delete();

        // delete the user
        return parent::delete();
    }

	public function page() {
		return $this->belongsTo('Fletch\Entities\Page');
	}

	public function content() {
		return $this->hasMany('Fletch\Entities\Content', 'element_id');
	}

	public function drafts() {
		return $this->hasOne('Fletch\Entities\ElementDraft', 'element_id')->orderBy('region', 'asc')->orderBy('position', 'asc');
	}

}

?>