<?php
namespace Fletch\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;
// application/models/draft.php
// The model for editable text or image elements within the CMS.

class ContentDraft extends Eloquent {
	protected $table = 'content_drafts';

	public function content() {
		return $this->belongsTo('Fletch\Entities\Content');
	}
}

?>