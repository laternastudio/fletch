<?php
namespace Fletch\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

// application/models/elements.php
// The model for editable text or image elements within the CMS.

class Content extends Eloquent {
	protected $table = 'content';

	public function elements() {
		return $this->belongsTo('Fletch\Entities\Element');
	}

	public function drafts() {
		return $this->hasOne('Fletch\Entities\ContentDraft', 'content_id');
	}

    public function delete()
    {
        // delete all related drafts 
        $this->drafts()->delete();

        // delete the user
        return parent::delete();
    }
}

?>