<?php
namespace Fletch\Entities;

use \Fletch\Elements\ElementLibrary;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Page extends Eloquent {

	protected $elements;
    protected $table = 'pages';
    protected $regions = [];
    public $elementLibrary = ;


    public function setElementLibrary(\Fletch\Elements\ElementLibrary $elementLibrary) {
        $this->elementLibrary = $elementLibrary;
    }
    /**
     * Get an array of element objects based on a given element library
     *
     * @return  array
     */
    public function getElements()
    {

        $auth = \Illuminate\Support\Facades\Auth::check();
        $elementObjects = array();

        foreach ($this->elements as $element) {
            $elementObjects[$element->region] = new $this->elementLibrary->elements[$element->type]($element, $auth);
        }

        return $elementObjects;
    }

    /**
     * Get the html output for the page
     *
     * @return  string
     */
    public function getRegionHtml($region)
    {
        $elements = $this->getElements();
        $elementArchitecture = [];

        foreach($elements as $element) {

        }

        foreach($elementArchitecture as &$region) {
            ksort($region);
            $region = implode($region);
        }
    }

    public static function load_routes()
    {
        $reserved_routes = array(
            0 => 'elements',
            1 => 'login',
            2 => 'logout',
            3 => 'drafts',
            4 => 'page',
            );

        $user_created_routes = Page::get(array('route'));

        foreach($user_created_routes as $user_created_route) {
            array_push($reserved_routes, $user_created_route->route);
        }

        return $reserved_routes;
    }


	public static function load_by_route($path)
    {
        $route = $path;

        // Set up the queries based on whether or not we're logged in
        // If we're not logged in, don't retrieve the element drafts

        // If we ARE logged in, ONLY retrieve the element drafts


        // Load the page
        $page = Page::with(array(
            'elements',
            'elements.drafts',
            'elements.content',
            'elements.content.drafts'
        ))->where('route', '=', $route)->first();

        //If the page was not found, return false
        if(is_null($page)) {
            return false;
        }
        else {



            return $page;
        }

    }


    /**
     * Get the content appropriate for a guest
     *
     * When a guest is logged in, only published content will be shown
     * which also means that deleted items are shown. On the other
     *
     * @return  array
     */
    // public function getGuestContent() {

 //        // If the element has been deleted, do not display it
 //        if(is_null($element->drafts) || (!is_null($element->drafts) && $element->drafts->deleted != '1')) {
 //            $element_html = \Render::element($element, $page->route);

 //            if(!is_null($element->drafts)) {
 //                $region = $element->drafts->region;
 //                $position = $element->drafts->position;                        
 //            }
 //            else {
 //                $region = $element->region;
 //                $position = $element->position;                       
 //            }

 //            // Currently hardcoded to only accept two-level position (i.e. 1.2)
 //            $page->target[$region][$position] = $element_html;
 //        }

 //        // Join the html targets into single blocks of html
 //        foreach($page->target as &$target) {
 //            ksort($target);
 //            $target = implode($target);
 //        }
    // }

    /**
     * Get the content appropriate for an administrator
     *
     * When an admin is logged in, an element will show drafted content,
     * which also means that deleted items are hidden.
     *
     * @return  array
     */
    // public function getAuthContent() {

 //        // Format the element data in to HTML
 //        foreach($this->elements as $element) {

 //            $region;
 //            $position;

 //            // Only show confirmed elements
 //            if($element->confirmed == '1') {
 //                $element_html = \Render::element($element, $page->route);

 //                // Currently hardcoded to only accept two-level position (i.e. 1.2)
 //                $page->target[$element->region][$element->position] = $element_html;              
 //            }

 //        }

 //        // Add the creators to each of the bodies of text

 //        // Join the html targets into single blocks of html
 //        foreach($page->target as &$target) {
 //            ksort($target);
 //            $target = implode($target);
 //        }
    // }


    public function delete()
    {
        // delete all related photos 
        foreach($this->elements as $element)
        {
            $element->delete();
        }

        // delete the user
        return parent::delete();
    }

	public function elements() {
		return $this->hasMany('Fletch\Entities\Element', 'page_id')->orderBy('region', 'asc')->orderBy('position', 'asc');
	}

}

?>