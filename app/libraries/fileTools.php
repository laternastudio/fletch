<?php

use Fletch\Entities\User as User;
use Fletch\Entities\Element as Element;
use Fletch\Entities\Content as Content;
use Fletch\Entities\ContentDraft as ContentDraft;
use Fletch\Entities\ElementDraft as ElementDraft;
use Fletch\Entities\Setting as Setting;
use Fletch\Entities\Page as Page;

class FileTools {
    
    /**
     * Ensure that the filename is safe and ready for the web.
     * 
     * @param  string  $filename      The full filename, including the extension.
     */
    public static function slugifyFilename($filename) {
        
        $path_parts = pathinfo($filename);
        
        // Proceed to slugify
        $filename = $path_parts['filename'];
        
        $basename = preg_replace('~[^\\pL\d]+~u', '-', $filename);
        $filename = trim($filename, '-');

        if (function_exists('iconv')) {
        	$filename = iconv('utf-8', 'us-ascii//TRANSLIT', $filename);
        }

        $filename = strtolower($filename);
        $filename = preg_replace('~[^-\w]+~', '', $filename);
        
        if (empty($filename)) {
        	$filename = 'unknown';
        }

        return $filename.'.'.$path_parts['extension'];
    	
    }
    
    public static function uniqueFilename($filename) {

        $unique_key = substr(sha1(strtotime("now")), 0, 8);
        $filename = $unique_key.'_'.$filename;
        
        return $filename;
    }
    
    /**
     * Force an upper resolution limit.
     * 
     * In some cases users may try to upload images with massive
     * resolutions. This function allows you to set an upper limit.
     * For example, you may say that the max resolution is 1920x1080.
     * In that case, uploaded images with a higher resolution will be
     * resized down (proportionally) to fit within those constraints.
     * 
     * @param  object  $image    An actual image resource using PHPImageWorkshop
     */
    public static function limitResolution($image) {
        
        $max_width   = 1920;
        $max_height  = 1080;
        $height      = $image->getHeight();
        $width       = $image->getWidth();
        
        if ($width > $max_width) {
            // Find the proportions
            $proportion = $max_width / $width;
            
            // Proportionally size both width and height
            $width  = $max_width;
            $height = $height * $proportion;
        }
        
        if ($height > $max_height) {
            // Find the proportions
            $proportion = $max_height / $height;
            
            // Proportionally size both width and height
            $height  = $max_height;
            $width = $width * $proportion;
        }
        
        $image->resizeInPixel($width, $height, false);
        return $image;
        
        //$image->save('upload/img/', $filename, false, false, 100);
            
        // I don't think I need this line
        // $image = PHPImageWorkshop\ImageWorkshop::initFromPath('upload/img/'.$filename);	
    }
    
    
     /**
     * Crop the image to the specified aspect ratio
     * 
     * @param  object  $image          An actual image resource using PHPImageWorkshop
     * @param  float   $aspect_ratio   A single number representation of the aspect ratio (width/height)
     */
    public static function cropToAspectRatio($image, $aspect_ratio) {

        // Portrait crop
        if ($aspect_ratio < 1) { 
            $crop_width = min($image->getHeight() * $aspect_ratio, $image->getWidth());
            $crop_height = $crop_width / $aspect_ratio;
        }
        // Landscape or square crop
        else {
            $crop_height = min($image->getWidth() / $aspect_ratio, $image->getHeight());
            $crop_width = $crop_height * $aspect_ratio;
        }

        $image->cropInPixel($crop_width, $crop_height, 0, 0, 'MM');
        
        return $image;
    }
    
    
    /**
     * Resize the image to one of the application preset sizes
     * 
     * @param  object  $image    An actual image resource using PHPImageWorkshop
     * @param  string  $size     The name of the size preset
     */
    public static function resizeToPreset($image, $size) {
        
        $presets = array(
            "any"    => 9999,
            "large"  => 800,
            "medium" => 550,
            "small"  => 350,
        );
        
        Log::info("Resizing to...".$presets[$size]);

        if ($image->getWidth() > $image->getHeight()) {
            // Don't resize up to a larger size.
            $resolution = min($presets[$size], $image->getWidth());
            
            // Perform the resize
            $image->resizeInPixel($resolution, null, true);
        }
        else {
            // Don't resize up to a larger size.
            $resolution = min($presets[$size], $image->getHeight());
            
            // Perform the resize
            $image->resizeInPixel(null, $resolution, true);
        }

        return $image;
    }
    
    /**
     * Add a string to the end of the filename (before the extension)
     * 
     * @param  string  $filename         the actual filename (i.e. thefile.png)
     * @param  string  $text_to_append   the text to append
     */
    public static function appendToFilename($filename, $text_to_append) {
		$path_parts = pathinfo($filename);
        
        $filename = $path_parts['filename'].$text_to_append.'.'.$path_parts['extension'];
        return $filename;
    }
    
}
?>