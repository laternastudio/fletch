<?php

use Fletch\Entities\User as User;
use Fletch\Entities\Element as Element;
use Fletch\Entities\Content as Content;
use Fletch\Entities\ContentDraft as ContentDraft;
use Fletch\Entities\ElementDraft as ElementDraft;
use Fletch\Entities\Setting as Setting;
use Fletch\Entities\Page as Page;

class Render {

    public static function youtubeThumbnail($code) {

        // Load the image resource
        $image_resource = Utility::loadImageFromUrl('http://img.youtube.com/vi/'.$code.'/sddefault.jpg');

        // Convert the image to a manageable data format
        $thumb_image = PHPImageWorkshop\ImageWorkshop::initFromResourceVar($image_resource);

        // Crop it to remove the black
        $thumb_aspect_ratio = 1.78;
        $thumb_crop_height = min($thumb_image->getWidth() / $thumb_aspect_ratio, $thumb_image->getHeight());
        $thumb_crop_width = $thumb_crop_height * $thumb_aspect_ratio;
        $thumb_image->cropInPixel($thumb_crop_width, $thumb_crop_height, 0, 0, 'MM');

        // Crop it to the correct gallery dimensions
        $thumb_aspect_ratio = 1.3;
        $thumb_crop_height = min($thumb_image->getWidth() / $thumb_aspect_ratio, $thumb_image->getHeight());
        $thumb_crop_width = $thumb_crop_height * $thumb_aspect_ratio;
        $thumb_image->cropInPixel($thumb_crop_width, $thumb_crop_height, 0, 0, 'MM');

        // Resize it a bit
        $thumb_image->resizeInPixel(350, null, true);

        $processed_image = $thumb_image->getResult('FFFFFF');

        ob_start ();

        imagejpeg($processed_image, null, 95);
        imagedestroy($image_resource);

        $data = ob_get_contents ();

        ob_end_clean ();

        $image_html = "<img src='data:image/jpeg;base64,".base64_encode ($data)."'>";

        return $image_html;
    }


    public static function sitemapToHtml($array, $addHome = false) {
        $out  = "<ul>";
        if ($addHome) {
            $out .= "<li><a href='".URL::to('/')."'>Home</a>";
        }

        foreach($array as $key => $elem){
            if(!is_array($elem)){
                // Do nothing
            }
            else {
                if($key != 'RS-fletch-data' && $key != '') {

                    // Build the link/non-link based on whether or not the data exists
                    if (array_key_exists('RS-fletch-data', $elem)) {
                        $link_html = "<a href='".$elem['RS-fletch-data']['url']."'>".$elem['RS-fletch-data']['title']."</a>";
                    }
                    else {
                        $link_html = Utility::titleCase(str_replace('-', ' ', $key));
                    }

                   // Generate the html and recurse as neccessary
                   $out .= "<li>$link_html".self::sitemapToHtml($elem)."</li>";

                }
            }
        }
        $out=$out."</ul>";
        return $out; 
    }

    public static function sitemap() {
        $pages = Page::all();
        $mappedPages = array();
        $workingMap = &$mappedPages;
        $html = '';

        foreach($pages as $page) {

            $html .= $page->route.'<br />';

            $route = explode('/', $page->route);

            foreach($route as $param) {

                if(!isset($workingMap[$param]) || !is_array($workingMap[$param])) {
                    $workingMap[$param] = array();
                }
                
                $workingMap = &$workingMap[$param];
            }

            $workingMap['RS-fletch-data'] = array();
            $workingMap['RS-fletch-data']['url'] = URL::to('/'.$page->route);
            $workingMap['RS-fletch-data']['title'] = $page->title;
            $workingMap = &$mappedPages;
        }

        // Add some additional pages
        $mappedPages['sitemap'] = array();
        $mappedPages['sitemap']['RS-fletch-data'] = array();
        $mappedPages['sitemap']['RS-fletch-data']['url'] = URL::to('/sitemap');
        $mappedPages['sitemap']['RS-fletch-data']['title'] = 'Sitemap';

        //$html .= '<pre>';
        //$html .= var_dump($workingMap);
        //$html .= '</pre>';

        return self::sitemapToHtml($mappedPages, true);
    }

    public static function socialmedia()
    {
        $settings = Setting::load_as_array();
        $html = '';

        if(isset($settings['facebook'])) {
            $html .= '<li><a href="'.$settings['facebook'].'"><i class="icon-facebook"></i></a></li>';
        }

        if(isset($settings['googleplus'])) {
            $html .= '<li><a href="'.$settings['googleplus'].'"><i class="icon-gplus"></i></a></li>';
        }

        if(isset($settings['youtube'])) {
            $html .= '<li><a href="'.$settings['youtube'].'"><i class="icon-youtube"></i></a></li>';
        }

        if(isset($settings['twitter'])) {
            $html .= '<li><a href="'.$settings['twitter'].'"><i class="icon-twitter"></i></a></li>';
        }

        if(isset($settings['instagram'])) {
            $html .= '<li><a href="'.$settings['instagram'].'"><i class="icon-instagram"></i></a></li>';
        }

        if(isset($settings['pinterest'])) {
            $html .= '<li><a href="'.$settings['pinterest'].'"><i class="icon-pinterest"></i></a></li>';
        }

        if(isset($settings['linkedin'])) {
            $html .= '<li><a href="'.$settings['linkedin'].'"><i class="icon-linkedin"></i></a></li>';
        }


        return $html;
    }

    public static function navigation($parent, $current_page = null , $single_level = true)
    {
        if($single_level)
        {
            $pages = Page::
                where('route', 'LIKE', $parent.'%')
                ->where('route', 'NOT LIKE', $parent.'%/%')
                ->where('visible', '=', '1')
                ->get();
        }
        else {
            $pages = Page::where('route', 'LIKE', $parent.'%')->get();            
        }

        $html = '';

        foreach($pages as $page) {

            // highlight current page
            if ((isset($current_page) && $current_page !== null) && $page->id == $current_page->id) {
                $html .= '<li class="current-page"><a href="#">'.$page->title.'</a></li>'; 
            }
            else {
                $html .= '<li><a href="'.URL::to($page->route).'">'.$page->title.'</a></li>';  
            }
        }

        return $html;
    }

    public static function element($element, $page)
    {
        


        $element_html; // Will store all of the element html
        $content = array(); // Initialized now so that it "exists" even if there is no content

        // Loads/replaces the drafts if the user is authorized
        foreach($element->content as $original_content) {
            array_push($content, $original_content);
            if(!is_null($original_content->drafts) && !Auth::guest()) {
                $content[count($content)-1]->data = $original_content->drafts->data;
            }
        }

        // Load up special class for elements, such as width
        $element_data = Utility::loadFromXML('../app/elements/'.$element->type.'.xml');
        $classes = "";

        foreach ($element_data->{'class'} as $class) {
            $classes .= " ".$class;
        }
        
        if($element->type == 'contact_form') {
            $element_html = self::contact_form($content[0]->data, $element->id, $content[0]->id);
        }
        elseif($element->type == 'newsletter_signup') {
            $element_html = self::newsletter_signup($content[0]->data, $element->id, $content[0]->id);
        }
        else {
            // Render the html based on its corresponding view
            $element_html = View::make('elements.'.$element->type, array('element' => $element, 'content' => $content, 'page' => $page, 'classes' => $classes));
        }
        
        // Add the movement and deletion controls if the user is logged in
        if(!Auth::guest()) {
            if(isset($content[0])) {
                $element_html = '<li id="element_'.$element->id.'" class="'.$classes.'" data-element-id="'.$element->id.'" data-content-id="'.$content[0]->id.'">'.$element_html.'<div class="fl-element-controls"><span class="move icon-move handle" alt="Move" title="Move"></span><span class="edit icon-pencil" alt="Edit" title="Edit"></span><span class="delete icon-trash" alt="Delete" title="Delete"></span></div></li>';
            }
            elseif (!isset($content[0])) {
                $element_html = '<li id="element_'.$element->id.'" class="'.$classes.'" data-element-id="'.$element->id.'" >'.$element_html.'<div class="fl-element-controls"><span class="move icon-move handle" alt="Move" title="Move"><span class="edit icon-pencil" alt="Edit" title="Edit"></span></span><span class="delete icon-trash" alt="Delete" title="Delete"></span></div></li>';
            }
        }

        return $element_html;
    }


    public static function element_by_id($id) {

        $element  = Element::where('id', '=', $id)->first();

        if(!is_null($element)) {
            $contents = $element->content()->get();

            if(Auth::check()) {
                foreach($contents as &$content) {

                    // Replace the data with the draft data if it exists
                    if($content->drafts()->first()) {
                        $draft = $content->drafts()->first();
                        $content->data = $draft->data;
                    }
                }            
            }


            // Render the html based on its corresponding view
            $element_html = View::make('elements.'.$element->type, array('element' => $element, 'content' => $content, 'page' => $page));
          
        }
        else {
            $element_html = '';
        }
        
        return $element_html;
    }

    public static function get_creator_position($region, $first, $second)
    {
        if(is_null($first)) {
            $first = ((int) $second / 2);
        }

        if(is_null($second)) {
            $second = ((int) $first + 2);
        }

        // Position number will be halfway between the other two elements
        $position = (int) $first + (((int) $second - (int) $first) / 2);

        return $region.'.'.$position;        
    }


    public static function creator($position)
    {
        $control_id = self::slugify('creator'.$position);
        $html = '<div id="'.$control_id.'" data-position="'.$position.'" class="creator">----------------</div>';
        return $html;
    }

    public static function slides()
    {
        $slides = Setting::where('name', '=', 'slide')->get();
        $html = '';

        foreach($slides as $slide) {
            $html .= '<li data-setting-id="'.$slide->id.'">';
            $html .=    '<img src="'.URL::to_asset('upload/img/'.$slide->value).'/" />';
            $html .= '</li>';
        }

        return $html;
    }

    public static function contact_form($text, $element_id, $content_id)
    {
        $html = '<div id="fletching_'.$element_id.'"';
        $html .= (Auth::check()) ? ' contenteditable="true" class="fl-editable" data-content-id="'.$content_id.'">' : '>';
        $html .= $text;
        $html .= '</div>';
        $html .= '<p style="color: green;">'.Session::get('contact_success').'</p>';
        $html .= '<p style="color: red;">'.Session::get('contact_error').'</p>';
        $html .= '<form method="POST" action="/sendmail">';
        $html .=    '<div class="row" style="min-width: 0 !important;">';
        $html .=        '<div class="sixcol">';
        $html .=            '<label for="name">Name:</label>';  
        $html .=            '<input type="text" id="name" name="name" value="'.Session::get('contact_name').'" placeholder="Enter your name...">';
        $html .=            '<label for="phone">Phone:</label>';  
        $html .=            '<input type="text" id="phone" name="phone" value="'.Session::get('contact_phone').'" >';
        $html .=        '</div>';
        $html .=        '<div class="sixcol last">';
        $html .=            '<label for="email">E-mail:</label>';  
        $html .=            '<input type="text" id="email" name="email" value="'.Session::get('contact_email').'" placeholder="your@address.com">';
        $html .=        '</div>';
        $html .=     '</div>';
        $html .=    '<div class="row" style="min-width: 0 !important; margin-bottom: 3em;">';
        $html .=        '<div class="twelvecol last">';
        $html .=            '<label for="message">Message:</label>';  
        $html .=            '<textarea name="message" id="message" placeholder="Enter your message...">'.Session::get('contact_message').'</textarea>';
        $html .=            '<input type="submit" value="Send Message">';
        $html .=        '</div>';
        $html .=     '</div>';
        $html .= '</form>';           

        return $html;
    }

    public static function newsletter_signup($text, $element_id, $content_id)
    {

        $html = '<div id="fletching_'.$element_id.'"';
        $html .= (Auth::check()) ? ' contenteditable="true" class="fl-editable" data-content-id="'.$content_id.'">' : '>';
        $html .= $text;
        $html .= '</div>';
        $html .= '<form id="inline_subscribe" class="subscribe" method="POST" action="/settings?type=emaillist">';
        $html .=    '<input type="text" id="inline_email_list_value" name="email" placeholder="your@address.com">';
        $html .=    '<input type="submit" id="inline_email_list_submit" value="Join">';
        $html .= '</form>';           
        return $html;
    }

    public static function slugify ($text)
    {

        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        $text = trim($text, '-');

        if (function_exists('iconv')) {
        	$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        }

        $text = strtolower($text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        // default output

        if (empty($text)) {
        	return 'n-a';
        }

        return $text;
    }
}




