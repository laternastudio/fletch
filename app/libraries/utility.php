<?php

use Fletch\Entities\Element as Element;
use Fletch\Entities\ContentDraft as ContentDraft;
use Fletch\Entities\ElementDraft as ElementDraft;

class Utility {

   
    
	public static function loadImageFromUrl( $url ){

		//Download images from remote server
		$image = imagecreatefromstring(file_get_contents($url));

        return $image;
	}

	public static function loadFromXML($path) {
		
		// Load up the file
		// $xmlStr = file_get_contents($path);
		// $xmlObj = simplexml_load_string($xmlStr);

		// Proccess it into the associative array
		$arrXml = simplexml_load_file($path);

		return $arrXml;
	}

	// Load all of the XML style attributes in the element XML files
	public static function loadElementStyles() {

		$style = array();

		// Cycle through all files in the elements directory
		foreach(glob("../app/elements/*.xml") as $filename) {
			$file = self::loadFromXML($filename);
			array_push($style, $file->style);
		}

		// Combine the styles
		$styles = implode('', $style);

		// Remove space after colons
		$styles = str_replace(': ', ':', $styles);
		 
		// Remove whitespace
		$styles = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $styles);

		return $styles;
	}

	// Load all of the XML style attributes in the element XML files
	public static function loadElementJavaScript() {

		$scripts = array();

		// Cycle through all files in the elements directory
		foreach(glob("../app/elements/*.xml") as $filename) {
			$file = self::loadFromXML($filename);

			if(!empty($file->javascript)) {
				$script = "element_js['$file->type'] = ".$file->javascript;

				array_push($scripts, $script);
			}
			
		}

		// Combine the javascript
		$scripts = 'var element_js = {};'.implode('', $scripts);

		// // Remove space after colons
		// $scripts = str_replace(': ', ':', $scripts);
		 
		// // Remove whitespace
		// $scripts = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $scripts);

		return $scripts;
	}

	public static function drafts_exist()
	{
		$new_elements   = Element::where('confirmed', '=', '0')->first();
	    $element_drafts = ElementDraft::first();
	    $content_drafts = ContentDraft::first();

	    $drafts = ($element_drafts == NULL
	    	&& $content_drafts == NULL
	    	&& $new_elements == NULL
	    	) ? false : true;

	    if($drafts) {
	    	Log::info('DRAFTS EXIST!');
	    }

	    return $drafts;
	}

	public static function youtube_id_from_url($url) {
		parse_str(parse_url( $url, PHP_URL_QUERY ), $vars );

		Log::info($vars['v']);
		return (isset($vars['v'])) ? $vars['v'] : false; 
	}



	// This function cleans out new line characters and/or other
	// special characters related to encoding issues
	public static function cleanStr($string) {

	    $string = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $string);

	    return $string;
	}


	//original Title Case script © John Gruber <daringfireball.net>
	//javascript port © David Gouch <individed.com>
	//PHP port of the above by Kroc Camen <camendesign.com>

	public static function titleCase ($title) {
		//remove HTML, storing it for later
		//       HTML elements to ignore    | tags  | entities
		$regx = '/<(code|var)[^>]*>.*?<\/\1>|<[^>]+>|&\S+;/';
		preg_match_all ($regx, $title, $html, PREG_OFFSET_CAPTURE);
		$title = preg_replace ($regx, '', $title);
		
		//find each word (including punctuation attached)
		preg_match_all ('/[\w\p{L}&`\'‘’"“\.@:\/\{\(\[<>_]+-? */u', $title, $m1, PREG_OFFSET_CAPTURE);
		foreach ($m1[0] as &$m2) {
			//shorthand these- "match" and "index"
			list ($m, $i) = $m2;
			
			//correct offsets for multi-byte characters (`PREG_OFFSET_CAPTURE` returns *byte*-offset)
			//we fix this by recounting the text before the offset using multi-byte aware `strlen`
			$i = mb_strlen (substr ($title, 0, $i), 'UTF-8');
			
			//find words that should always be lowercase…
			//(never on the first word, and never if preceded by a colon)
			$m = $i>0 && mb_substr ($title, max (0, $i-2), 1, 'UTF-8') !== ':' && 
				!preg_match ('/[\x{2014}\x{2013}] ?/u', mb_substr ($title, max (0, $i-2), 2, 'UTF-8')) && 
				 preg_match ('/^(a(nd?|s|t)?|b(ut|y)|en|for|i[fn]|o[fnr]|t(he|o)|vs?\.?|via)[ \-]/i', $m)
			?	//…and convert them to lowercase
				mb_strtolower ($m, 'UTF-8')
				
			//else:	brackets and other wrappers
			: (	preg_match ('/[\'"_{(\[‘“]/u', mb_substr ($title, max (0, $i-1), 3, 'UTF-8'))
			?	//convert first letter within wrapper to uppercase
				mb_substr ($m, 0, 1, 'UTF-8').
				mb_strtoupper (mb_substr ($m, 1, 1, 'UTF-8'), 'UTF-8').
				mb_substr ($m, 2, mb_strlen ($m, 'UTF-8')-2, 'UTF-8')
				
			//else:	do not uppercase these cases
			: (	preg_match ('/[\])}]/', mb_substr ($title, max (0, $i-1), 3, 'UTF-8')) ||
				preg_match ('/[A-Z]+|&|\w+[._]\w+/u', mb_substr ($m, 1, mb_strlen ($m, 'UTF-8')-1, 'UTF-8'))
			?	$m
				//if all else fails, then no more fringe-cases; uppercase the word
			:	mb_strtoupper (mb_substr ($m, 0, 1, 'UTF-8'), 'UTF-8').
				mb_substr ($m, 1, mb_strlen ($m, 'UTF-8'), 'UTF-8')
			));
			
			//resplice the title with the change (`substr_replace` is not multi-byte aware)
			$title = mb_substr ($title, 0, $i, 'UTF-8').$m.
				 mb_substr ($title, $i+mb_strlen ($m, 'UTF-8'), mb_strlen ($title, 'UTF-8'), 'UTF-8')
			;
		}
		
		//restore the HTML
		foreach ($html[0] as &$tag) $title = substr_replace ($title, $tag[0], $tag[1], 0);
		return $title;
	}

}

?>