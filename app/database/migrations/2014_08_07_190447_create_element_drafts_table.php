<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElementDraftsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('element_drafts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('element_id');
			$table->integer('region');
			$table->integer('position');
			$table->string('type', 128);
			$table->tinyInteger('deleted');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('element_drafts');
	}

}
