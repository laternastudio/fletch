<?php

use Fletch\Entities\Element as Element;

class ElementsTableSeeder extends Seeder {

	public function run() {

		/**
		 * Create some generic text elements on the home page
		 */

		// id will be 1
		Element::create([
				'page_id' => 1,
				'region' => 0,
				'position' => 0,
				'type' => 'div',
				'confirmed' => 1
			]);

		// id will be 2
		Element::create([
				'page_id' => 1,
				'region' => 1,
				'position' => 0,
				'type' => 'div',
				'confirmed' => 1
			]);

		// id will be 3
		Element::create([
				'page_id' => 1,
				'region' => 2,
				'position' => 0,
				'type' => 'div',
				'confirmed' => 1
			]);

		/**
		 * Create some generic text elements on the about page
		 */

		// id will be 4
		Element::create([
				'page_id' => 2,
				'region' => 0,
				'position' => 0,
				'type' => 'div',
				'confirmed' => 1
			]);

		// id will be 5
		Element::create([
				'page_id' => 2,
				'region' => 1,
				'position' => 0,
				'type' => 'div',
				'confirmed' => 1
			]);

		// id will be 6
		Element::create([
				'page_id' => 2,
				'region' => 2,
				'position' => 0,
				'type' => 'div',
				'confirmed' => 1
			]);

		/**
		 * Create some generic text elements on the contact page
		 */

		// id will be 7
		Element::create([
				'page_id' => 3,
				'region' => 0,
				'position' => 0,
				'type' => 'div',
				'confirmed' => 1
			]);

		// id will be 8
		Element::create([
				'page_id' => 3,
				'region' => 1,
				'position' => 0,
				'type' => 'div',
				'confirmed' => 1
			]);

		// id will be 9
		Element::create([
				'page_id' => 3,
				'region' => 2,
				'position' => 0,
				'type' => 'div',
				'confirmed' => 1
			]);

	}
}