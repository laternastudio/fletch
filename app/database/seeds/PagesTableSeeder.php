<?php

use Fletch\Entities\Page as Page;

class PagesTableSeeder extends Seeder {

	public function run() {

		$faker = Faker\Factory::create();

		// Create the homepage
		Page::create([
				'route' => '/',
				'title' => 'Home',
				'long_title' => 'Fletch Content Management System',
				'description' => 'Fletch is a modern content management system that makes website management incredibly simple.',
				'layout' => 'twocol_single',
				'visible' => 1,
				'disabled' => 'route|delete|layout'
			]);

		// Create a couple more generic pages
		Page::create([
				'route' => 'about-us',
				'title' => 'About',
				'long_title' => 'About Us',
				'description' => $faker->sentence(14),
				'layout' => 'twocol_single',
				'visible' => 1
			]);

		// Create a couple more generic pages
		Page::create([
				'route' => 'contact',
				'title' => 'Contact',
				'long_title' => 'Contact Us',
				'description' => $faker->sentence(14),
				'layout' => 'twocol_single',
				'visible' => 1
			]);
	}
}