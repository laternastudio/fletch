module.exports = function(grunt) {
	grunt.initConfig({
		compass: {                  // Task
			dist: {                   // Target
				options: {              // Target options
					sassDir: 'scss',
					cssDir: 'css',
					environment: 'development'
				}
	    	}
	    },
		uglify: {
			options: {
				banner: '/*! Fletch CMS User Javascript <%= grunt.template.today("yyyy-mm-dd") %> */\n',
			},
			dist: {
			    files: {
			       'js/main.min.js': ['js/components/main/*.js'],
			       'js/user.min.js': ['js/components/user/*.js']
			    }
			}

	    },
		concat: {
			dist: {
			    files: {
			       'js/main.min.js': ['js/components/main/*.js'],
			       'js/user.min.js': ['js/components/user/*.js']
			    }				
			}
	    },
		watch: {
			files: ['js/components/**/*.js', 'scss/*.scss', 'scss/fletch/*.scss', 'scss/theme/*.scss', 'scss/elements/*.scss'],
			tasks: ['uglify', 'compass', 'concat'],
			options: {
			    livereload: true,
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-concat');
};