/*!
 * ElementOnChange
 * Written by Pete Molinero of Laterna Studio
 * http://www.laternastudio.com
 * All Rights Reserved.
 *
 * Synopsis:
 * This class handles the actions that take place when a hidden input for an element changes.
 */

var ElementOnChange = {


	init: function () {
		this.navigation();

	},


	// When the navigation of a page is updated, the page simply reloads
	navigation: function () {

		$(document).on('change', 'nav.sub-navigation input.option', function()
		{
			location.reload(true);
		});
	},


};