/*!
 * A Utility Module
 * Written by Pete Molinero of Laterna Studio
 * http://www.laternastudio.com
 * All Rights Reserved.
 *
 * Synopsis:
 * A Utility module that includes things such as alert boxes, confirmation boxes, etc.
 * These modals are based on jQuery UI modals.
 */


/*
	USAGE:

	var yourChoice = utility.confirm(
		'What do you say?',
		'Delete Everything?',
		'icon-attention',
		function() {
			utility.alert('All items deleted.', 'Delete Complete', 'icon-ok');
		}
	});

*/

var Utility = {

	// A basic alert box
	// message: a string that includes the alert message
	// title:   a string that includes the box title
	// icon:    a string that includes the fontello class for the icon. Defaults to icon-attention.
	alert: function(message, title, icon) {

		var modal_id,
			modal_html;

		// Defaults to an attention icon.
		icon = typeof icon !== 'undefined' ? icon : 'icon-attention';

		// Generate an ID for the modal to use
		modal_id = this.generateKey(8);

		// Create the hidden modal window, hidden with generated ID
		modal_html  = '<div id="' + modal_id + '" class="modal" title="' + title + '">';
		modal_html  += '<i class="' + icon + '"></i><p>';
		modal_html  += message + '</p></div>';

		// Insert the modal window HTML
		$('body').append(modal_html);

		// Activate the modal window
        $('#' + modal_id).dialog({
            modal: true,
            buttons: {
                Ok: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
	},

	// A basic confirmation box
	// message:  a string that includes the alert message
	// title:    a string that includes the box title
	// icon:     a string that includes the fontello class for the icon. Defaults to icon-attention.
	// callback: the function that is called if "Okay" is selected
	confirm: function(message, title, icon, callback) {

		var modal_id,
			modal_html;

		// Defaults to an attention icon.
		icon = typeof icon !== 'undefined' ? icon : 'icon-attention';

		// Generate an ID for the modal to use
		modal_id = this.generateKey(8);

		// Create the hidden modal window, hidden with generated ID
		modal_html  = '<div id="' + modal_id + '" class="modal" title="' + title + '">';
		modal_html     += '<i class="' + icon + '" ></i><p>';
		modal_html     += message + '</p></div>';

		// Insert the modal window HTML
		$('body').append(modal_html);

		// Activate the modal window
		$('#' + modal_id).dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Okay": function() {
                    $( this ).dialog( "close" );
                    callback();
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                    return false;
                }
            }
        });
    },

	// A basic text input modal
	// message:     a string that includes the alert message
	// title:       a string that includes the box title
	// icon:        a string that includes the fontello class for the icon. Defaults to icon-attention.
	// callback:    the function that is called if "Okay" is selected
	// placeholder: the placeholder text for the input box
	// submit_text: the text for the "submit" button
	entryModal: function(message, title, icon, placeholder, submit_text, value, callback) {
		
		var modal_id,
			modal_html;

		// Defaults to no icon.
		icon = typeof icon !== 'undefined' ? icon : '';

		// Defaults to no value.
		value = typeof value !== 'undefined' ? value : '';

		// Generate an ID for the modal to use
		modal_id = this.generateKey(8);

		// Create the hidden modal window, hidden with generated ID
		modal_html  = '<div id="' + modal_id + '" class="modal" title="' + title + '">';

		// Only use an icon if it is defined
		if (icon != '') {
			modal_html += '<i class="' + icon + '" ></i>';
		}
		
		modal_html += '<p>' + message + '</p><input type="text" placeholder="' + placeholder + '" name="entryModal" id="entryModal" /></div>';

		// Insert the modal window HTML
		$('body').append(modal_html);

		// Insert the starting value, if there is one
		$('input#entryModal').val(value);

		// Activate the modal window
		$('#' + modal_id).dialog({
            resizable: false,
            modal: true,
            buttons: [{
				text: submit_text,
                click : function() {
					var data = $('input#entryModal').val();
                    $( this ).dialog( "close" );
                    console.log('The entry modal value is: ' + data);
                    callback(data);
                    $('#' + modal_id).remove();
                },
            },  {
				text: 'Cancel',
                click: function() {
					$( this ).dialog( "close" );
					$('#' + modal_id).remove();
					return false;
                },
            }],
        });
	},

	// Generate a random string of letters and numbers
	// len: the number of characters the key will be
	generateKey: function(len) {

		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for( var i=0; i < len; i++ )
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		return text;
	},

	// Find the top and left values to center one element inside another
	// parent: The element that will act as the container
	// child:  The element that should be centered
	centerCoords: function(parent, child) {

		var parent_width  = parent.innerWidth();
		var parent_height = parent.innerHeight();

		var child_width  = child.outerWidth();
		var child_height = child.outerHeight();

		var top  = (parent_height / 2) - (child_height / 2);
		var left = (parent_width / 2) - (child_width / 2);

		// Return an object with .top and .left
		var coords = {'top' : top, 'left' : left};

		return coords;	
	}
};