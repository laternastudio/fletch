/*!
 * CKEditorAJAX
 * Written by Pete Molinero of Laterna Studio
 * http://www.laternastudio.com
 * All Rights Reserved.
 *
 * Synopsis:
 * Setup of the CKEDITOR functionality needed to deal with the AJAX saving of inline edits.
 */

	CKEditorAJAX = {

	tmp_text: '',
	editor_open: false,

	init: function() {
		this.listen();
	},

	listen: function() {
		CKEDITOR.on( 'panelDisplayed', function( event ) {
			CKEditorAJAX.onDisplay(event);
		});

		CKEDITOR.on( 'panelHidden', function( event ) {
			CKEditorAJAX.onHidden(event);
		});
	},

	// The method that runs when an inline editing panel is displayed
	onDisplay: function(event) {

		// Expose the status of the editing panel to the rest of the app
		CKEditorAJAX.editor_open = true;

		// Hide element controls so that they don't overlap text
		Ui.hideElementControls();

		// Grab the editor instance
		var editor = event.editor,
		element = editor.element;

		// Save the text so that we can tell if it changed.
		CKEditorAJAX.tmp_text = $(element.$).html();		
	},

	// The method that runs when an inline editing panel is hidden
	onHidden: function(event) {
		// Expose the status of the editing panel to the rest of the app
		CKEditorAJAX.editor_open = false;

		// Grab the editor instance
		var editor = event.editor,
		element = editor.element;

		// Check to see whether or not the text content has actually been changed.
		// Only save it to the database if there are, indeed, changes.
		if(CKEditorAJAX.tmp_text != $(element.$).html()) {

			var id = ($(element.$).closest('li').attr('id') !== null) ? $(element.$).closest('li') : $(element.$).parent();

			var edited_element = $(element.$);
			var element_id     = id.attr('data-element-id');
			var content_id     = edited_element.attr('data-content-id');
			var data       = edited_element.html();

			Database.update_content(element_id, content_id, 'text', { data: data});
		}		
	},
};