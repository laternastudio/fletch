/*!
 * Database Interface
 * Written by Pete Molinero of Laterna Studio
 * http://www.laternastudio.com
 * All Rights Reserved.
 *
 * Synopsis:
 * Handles the JS interactions with the Laraval controllers
 */

var Database = {

	// Create a Fletch element
	// page:   the database id of the page that will recieve the new element
	// region: the region that the new element will be added to
	// type:   the type of element that will be created (i.e. 'div', 'figure', 'etc.')
	// data:   the starting data for the element. (i.e. text, image url, etc.)
	create_element: function(page, region, type, data) {

		Ui.setNotificationMessage('Saving...');

		var jqxhr = jQuery.post( '/elements', {
			page: page,
			region: region,
			type: type,
			data: data},

			// The callback function
			function(data){

				// Proceed if ElementController.php reported that the element was
				// successfully created. Four variables will be returned:
				//
				//		data.success:    a string that is 'true' or 'false'  
				//		data.message:    the success message
				//		data.html:       the html for the element, to be inserted into the DOM
				//		data.element_id: the database id of the element

                // Show the publish buttons and success message
                Ui.showPublishPanel();
                Ui.setNotificationMessage('All changes saved.');

                // Insert the element into the DOM
                $('#region-' + region + ' .fl-region-list').append(data.html);

                // Reboot the repositioning system so that it includes the new element
                Reposition.reboot();

                // Activate the editability of any images that are part of the new element
                $('li#element_' + data.element_id).find('img').load(function(){

                    // Grab the image's id
                    var img_id = $(this).attr('id');

                    // Create and initialize the new editable image
                    editableImages[img_id] = new EditableImage(img_id);
                    editableImages[img_id].init();

                });

                // Grab the "potential" inline editable text
                // and activate it if it has the editable class.
                var potentialInline = $('#fletching_' + data.element_id);

                if(potentialInline.hasClass('fl-editable')) {
                    CKEDITOR.inline(potentialInline.attr('id'));
                }

		}, 'json')
        // In case the update operation failed.
        .fail(function() {
            Ui.setNotificationMessage('Sorry, an error occured.');
        });
	},

    /**
     * 
     * Update something that pertains to an entire element. Specifically, this method
     * allows you to save a new 'position' for an element or flag an element for deletion.
     * 
     * @param {string}   element_id - The database row id of the element that the content belongs to
     * @param {string}   type       - Specify either 'position' or 'destroy'
     * @param {object}   params     - Any additional parameters you'd like to post to the controller. For position, pass in something like: { region: 1, position: 3}.
     * @param {function} success    - The function that should execute upon success
     * 
     */
	update_element: function(element_id, type, params, success) {

		Ui.setNotificationMessage('Saving...');
		success = typeof success !== 'undefined' ? success : function() {};

        // Fix size of all editable images, since they can
        // get messed up by the repositioning into a different column
        $('.editableImage').each(function(index) {
            $(this).parent().height($(this).height());
        });

        // Save the updated position of the element
        var jqxhr = jQuery.post( '/elements/' + element_id + '?type=' + type, { _method: 'PUT', region: params.region, position: params.position}, function(data){

            success();

            // Now that we've saved a draft, we'll show the publish and undo buttons
            Ui.showPublishPanel();

            // And the returned completion message
            Ui.setNotificationMessage('All changes saved.');

        })
        // In case the update operation failed.
        .fail(function() {
            Ui.setNotificationMessage('Sorry, an error occured.');
        });			

	},
    
    /**
     * 
     * Update one of the pieces of content inside an element. This could be a
     * url, a caption, a body of text, an image, or many other types of content.
     * There are, however, two primary distinctions: text or image. You must
     * specify which kind of content you are updating via the type parameter.
     * 
     * @param {string}   element_id - The database row id of the element that the content belongs to
     * @param {string}   content_id - The database row id of the content
     * @param {string}   type       - Specify either 'text' or 'image'.
     * @param {object}   params     - Any additional parameters you'd like to post to the controller. For text, pass in something like: { data: "Some String"}.
     * @param {function} success    - The function that should execute upon success
     * 
     */
    update_content: function(element_id, content_id, type, params, success) {
            
        Ui.setNotificationMessage('Saving...');
        success = typeof success !== 'undefined' ? success : function() {};

        if(type == 'text') {
   
            // Save the updated text
            var jqxhr = jQuery.post( '/elements/' + element_id + '/' + content_id + '?type=' + type, { _method: 'PUT', data: params.data }, function(data){

                success();
                
                // Now that we've saved a draft, we'll show the publish and undo buttons
                Ui.showPublishPanel();

                // And the returned completion message
                Ui.setNotificationMessage('All changes saved.');

            }, 'json')
            // In case the update operation failed.
            .fail(function() {
                Ui.setNotificationMessage('Sorry, an error occured.');
            });	
        }
        else if ( type == 'image') {
            
            /*
             * Because this submits an image with it, we have to use
             * the jquery ajaxform. This causes the submission process
             * to be more complex that just the $.post used in the other
             * types of updates.
             */
            
            var image_object = params.image_object;
            
			// Set up the specific AJAX options for this submit.
            options = {
                data: { 
                    _method: 'PUT',
                    type: 'image',
                    aspect_ratio: image_object.aspect_ratio,
                    size:         image_object.size,
                },
                dataType: 'json',
                success:  function(data) {

                    image_object.completeUpload(data);
                    Ui.setNotificationMessage('All changes saved.');

                },
                error:  function(t) { 
                    image_object.failUpload(data);
                    Ui.setNotificationMessage('Sorry, an error occured.');
                }
            };
            
            // Submit the form
            $('#form_' + image_object.id).ajaxSubmit(options);
        }
    },
    

	// Delete an element from the database
	// element_id: the actual row id of the element in the database
	delete_element: function(element_id) {

		Ui.setNotificationMessage('Saving...');
		
		var html_element = $("#element_" + element_id);

		// jQuery saving of the element.
		var jqxhr = jQuery.post( '/elements/' + element_id + '?type=destroy', { _method: 'PUT'}, function(data){
			html_element.remove();
			Ui.showPublishPanel();
			Ui.setNotificationMessage('All changes saved.');
		})
        // In case the update operation failed.
        .fail(function() {
            Ui.setNotificationMessage('Sorry, an error occured.');
        });				
	}


};