/*!
 * Element Creator Module
 * Written by Pete Molinero of Laterna Studio
 * http://www.laternastudio.com
 * All Rights Reserved.
 *
 * Synopsis:
 * This module deals with the element creator--the "Add an Item" widget
 * in Fletch that allows you to add elements onto your page.
 */

var ElementCreator = {

	// Initialize
	init: function() {
		this.addCreationBar();
		this.listen();
	},

	// Create the 'Add an Item' bar in each region
	addCreationBar: function() {

		var region,
			addAnItemHtml;

		$("div.region").each(function() {

			// Figure out our current region
			region = $(this).data('region');

			// Create the "Add an Item" button
			addAnItemHtml = '<br style="clear: both;" /><a id="region-' + region + '-creator" class="fl-creator icon-plus" data-dropdown="#creator-dropdown" href="#">ADD AN ITEM</a>';
			$(this).append(addAnItemHtml);

			// We don't need to "listen" to this button because the dropdown script works automatically on it.

		});
	},

	// Begin listening to each of the creator dropdown options
	listen: function() {
		$('#creator-dropdown').find('a').on('click', function(e) {
			e.preventDefault();
			ElementCreator.activate(this);
		});		
	},

	// Activate this when someone clicks to create an element
	// Such as 'Add an Item >> Text'
	activate: function(obj) {

		// Initialize the anchor element as a jQuery object
		var anchor = $(obj);

		// Identify element type
		var elementType = anchor.attr('data-element-type');

		// Find out which page the element should be added to
		page   = $('#global_page_id').text();

		// Traverses up the DOM tree to find out which region it needs to add to
		// Note that data-trigger is added by the dropdown JS 
		var trigger = anchor.closest('div').attr('data-trigger');
		region = trigger.split('-')[1];

		// Initiallize variable to use for
		var data = {};

        // element_js has been created as inline Javascript via PHP when the page was loaded.
        // It is taken from the XML files for the elements and output in the head section of the page.
        // This is just saying "if there is javascript for the element, use it. Otherwise just create the element."
		if (elementType in element_js) {

			var callback = function(data) { Database.create_element(page, region, elementType, data); };
			element_js[elementType](callback);
		}
		else {
			Database.create_element(page, region, elementType, []);
		}

		
		//Database.create_element(page, region, elementType, data);
		
	},
};
