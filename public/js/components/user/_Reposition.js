/*!
 * Reposition
 * Written by Pete Molinero of Laterna Studio
 * http://www.laternastudio.com
 * All Rights Reserved.
 *
 * Synopsis:
 * This class manages all of the drag and drop content repositioning via jQuery UI.
 */

var Reposition = {

	dragging: false,
	currentItem: 'null',
	placeholderItem: 'null',
	placeholderParent: 'null',
	item_w: 0,
	item_h: 0,

	init: function () {

		// Set up the sortable list
		$('.fl-region-list:visible').sortable({
			handle: '.handle',
			connectWith: '.fl-region-list',
			forcePlaceholderSize: true,
			scroll: true,
			tolerance: 'pointer',
			cursor: 'move',
			placeholder: 'fl-reposition-placeholder',

			start: function(event, ui) {

				// Set up all the configuration
				Reposition.dragging = true;
				Reposition.currentItem = ui.item;
				Reposition.placeholderItem = ui.placeholder;
				Reposition.saveDimensions(ui.item);
				Reposition.placeholderParent = ui.placeholder.parent().parent().attr('id');
				Reposition.hidePlaceholder();
				Reposition.resizeItem(ui.item);

				// Fix repositioning bug
				Reposition.currentItem.parent().append(Reposition.currentItem);

				// Refresh to make sure all the dragging hotpots are correct
				$('.fl-region-list').sortable('refresh');
			},

			change: function(event, ui) {

				$('.fl-region-list').sortable('refresh');

		    	// Only execute this code if the placeholder moves into a new region
		    	if( Reposition.isNewRegion() || Reposition.placeholderItem.width() == 0 ) {
					
					// Set the correct placeholder size
					Reposition.resizePlaceholder();	    		
		    		
				}
			},

			stop: function(event, ui) {
				Reposition.dragging = false;
				Reposition.resizeImages();
			}

		}).bind('sortupdate', function(e) {

			// Now that something has been moved, we need to resave all of the positions,
			// because the removal of the item from one column caused item positions to
			// change, and the insertion of it into a new column caused items to change.
			Reposition.savePositions();
		});
	},

	// This function is used when a new item is added.
	// It is basically a reboot of the whole system and includes new items.
	reboot: function () {
		$('.fl-region-list').sortable('destroy');
		this.init();
	},

	// Resave all of the positions
	savePositions: function() {
		$('.fl-region-list li').each(function() {

			// Grab the list element's html id
			var html_element = $(this);

			element_id = html_element.attr('data-element-id');
			region = html_element.parent().parent().attr('data-region');
			position = html_element.index();

			// Save the new position
			Database.update_element(element_id, 'position', { region: region, position: position });

		});
	},

	isNewRegion: function() {
		if (Reposition.placeholderParent != Reposition.placeholderItem.parent().parent().attr('id')) {

			// Update the placeholderParent
			Reposition.placeholderParent = Reposition.placeholderItem.parent().parent().attr('id');
			return true;
		}
		else {
			return false;
		}
	},

	// Save the original dimensions of the item
	saveDimensions: function(item) {
		Reposition.item_w = item.width();
		Reposition.item_h = item.height();
	},

	resizeItem: function(item) {
		var w = item.outerWidth(); // = 10
		var h = item.outerHeight(); // = 20
		var aspect_ratio = h / w; // = 0.5

		// Always set to 300 width
		// If the height is long, so be it
		// Eventually I'd like the max-height to be 300 and
		// have it fade out if it was longer than that
		item.width(300); 
		item.height(aspect_ratio * 300);
		Reposition.resizeImages();

	},

	// Images inside dragged items need to be resized manually
	resizeImages: function() {

		Reposition.currentItem.find('img').each(function() {

			var img_id = $(this).attr('id');
			
			// Update the image size if it is an editable image
			if ( img_id in editableImages) {
				editableImages[img_id].resize();
			}
		});
		
	},

	hidePlaceholder: function() {
		Reposition.placeholderItem.width(0);
		Reposition.placeholderItem.height(0);
	},

	resizePlaceholder: function() {

		// See if the element has any special non-full size
		var size = Reposition.currentItem.attr('class');
		size = size.trim();

		// The first class will be the size
		size = size.split(' ')[0];

		// Calculate the new height
		// Calculate the parent containers width. This is neccesary for percentages.
		var parent_width = parseFloat(Reposition.currentItem.parent().css('width'));

		// Placeholder width
		var placeholder_parent_px_width = parseFloat(Reposition.placeholderItem.parent().width());

		// Find out what the elemtent's new height should be
		var old_px_width = parseFloat(Reposition.item_w);
		var percent_width = 100 * old_px_width / parent_width;
		var new_px_width = placeholder_parent_px_width * (percent_width / 100);
		var px_height = parseFloat(Reposition.item_h);
		var aspect_ratio = px_height / old_px_width;
		var new_px_height = (new_px_width * aspect_ratio) + 'px';

		// If there is a size class, apply it to the placeholder
		if (size == 'fl-xml-quarter') {

			// Clear the CSS styling
			Reposition.placeholderItem.css('width', '');
			Reposition.placeholderItem.addClass('fl-xml-quarter');

			// Apply the correct height to the placeholder
			Reposition.placeholderItem.height(new_px_height);			
		}
		else {
			Reposition.placeholderItem.width('100%');

			// Apply the correct height to the placeholder
			Reposition.placeholderItem.height('1em');
			Reposition.placeholderItem.css('margin', '1em 0');
		}		
	}


};