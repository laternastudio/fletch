


// Set up the editable images
$(window).load(function() {
    $('.editableImage').each(function(index) {
		// create an array entry for each.
		editableImages[$(this).attr('id')] = new EditableImage($(this).attr('id'));

		// Set up the image for editing
		editableImages[$(this).attr('id')].construct();
		editableImages[$(this).attr('id')].listen();
    });

	// Unfortunately a resize handler was used to ensure the div
	// keeps up the same height as the image inside. Thankfully,
	// this is unlikely to be called often.
	$(window).resize(function() {
		$('.editableImage').each(function(index) {
			$(this).parent().height($(this).height());
			console.log('Resizing by jQuery.');
		});
	});
});

//*************************************************
// Instantiate object literals
//*************************************************

$(function() {

	// Initialize the ui
	Ui.init();
	CKEditorAJAX.init();
	ElementCreator.init();
	Reposition.init();
	ElementOnChange.init();
});