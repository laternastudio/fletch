/*!
 * Mobile Navigation
 * Written by Pete Molinero of Laterna Studio
 * http://www.laternastudio.com
 * All Rights Reserved.
 *
 * Synopsis:
 * This is the expanding navigation that appears when the user's
 * browser has a small width--such as on a phone.
 */

 var MobileNav = {	

 	// Set up the connections to the html elements
	trigger: '#mobile-nav-trigger',
	menu: 'nav.mobile',
	menuHeight: $('nav.mobile').height(), 

 	// The initialization method
 	init: function() {
 		this.listenOnTrigger();
 		this.closeOnResize();
 	},

 	// Close the menu if the window is resized
 	closeOnResize: function() {
 		$(window).resize(function(){
			var w = $(window).width();

			if(w > 767 && $(MobileNav.trigger).hasClass('close')) {
				console.log('Inside...');
				$(MobileNav.menu).hide();
				$(MobileNav.trigger).toggleClass('menu-tip').toggleClass('close');
			}
		});
 	},

 	// Listen on the menu trigger
 	listenOnTrigger: function() {

		$(MobileNav.trigger).on('click', function(e) {

			// Make the navigation trigger doesn't act like a real link/anchor
			e.preventDefault();

			// So that we have a nice smooth menu open
			$('nav.mobile').animate({
				'opacity': 'toggle',
				'height': 'toggle'
			}, 100);

			// This is so that the trigger changes styles when the menu is open
			$(this).toggleClass('close');
		});
 	},
 }